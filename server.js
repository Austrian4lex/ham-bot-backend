/************ DEFINE PACKAGE AND OTHER STUFF ************/

// define other javascript files
const settings = require('./settings');
const GRBL = require('./controllers/GrblController');
const UserLogin = require('./controllers/UserController');
const SerialData = require('./controllers/SerialDataController');

// define routes
var api = require('./routes/api');

// define packages
const async = require('async');
const bodyParser = require('body-parser');
const exec = require('child_process').exec;
const express = require('express');
const express_session = require('express-session');
const fs = require('fs');
//const GPIO = require('rpi-gpio');
const http = require('http');
const logger = require('morgan');
const methodOverride = require('method-override');
const MongoStore = require('connect-mongo')(express_session);
const path = require('path');
const readline = require('readline');
const SerialPort = require('serialport');
const WebSocket = require('ws');
const url = require("url");


// Setup for Readline
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// initialization for express
const app = express();
// initialization for http server
const server = http.createServer(app);




/************ INITIALIZATIONS FOR EXPRESS ************/


// use public folder
app.use(express.static(path.join(__dirname, 'public')));

// define some app stuff
app.use(logger('dev'));
app.use(methodOverride());
app.use(express_session({
    secret: 'HAM_Bot_secret',
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({
      url: 'mongodb://127.0.0.1/ham_bot',
      touchAfter: 3 * 24 * 60 * 60
    }),
    cookie: { secure: false }
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Fix Access-Control-Allow-Origin
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", settings.webserverIP);
  res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Access-Control-Request-Methods, Accept, Authorization, Content-Type, Origin, X-Requested-With");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  next();
});

// define routes
app.use('/api', api);
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname,'public/index.html'));
});

/*
GPIO.setup(7, GPIO.DIR_LOW, writeRedLED); // red LED setup

function writeRedLED(status) {
  GPIO.write(7, status, (error) => {
    if (error) log(error);
  });
}

*/

/************ SERVER ERROR HANDLING ************/

// error handlers

// development error handler, will print stacktrace
if (app.get('env') === 'development') {
    app.use((error, req, res, next) => {
        res.status(error.status || 500);
        res.render('error', {
            title: 'HAM-Bot | Error',
            message: error.message,
            error: error
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    log(error);
    res.send({ message: "Unhandeled Error: " + error });
});

// Function to start server and listen to port 80
function startServer() {
  server.listen(settings.webserverPort, () => {
    //GPIO.setup(13, GPIO.DIR_HIGH); // 2. green on
    log('Listening on: ' + server.address().port);
  });
}


// Server Listen + First setup if needed
setTimeout(() => {
  UserLogin.getAllUsers((response) => {
    if (response.users[0]) {
      startServer();
    } else {
      var username = "";
      var password = "";

      console.log('No User found in DB to log in to website!\nYou need to create one!');
      async.series([
          (callback) => {
              rl.question('Username (default would be "Admin"): ', (input) => {
                  username = input;
                  callback();
              })
          },
          (callback) => {
              rl.question('Password (default would be "password"): ', (password) => {
                  password = password;
                  callback();
              })
          },
      ], () => {
          rl.close();

          if (!username || username == "")
            username = "Admin";

          if (!password || password == "")
            password = "password";

          // No need to check for existing User -> DB is empty
          UserLogin.register(username, password, (response) => {
            log(response.message);
            if (response.success)
              return startServer();
            return;
          });
      }); //async end
    } //else end
  }); // getAllUsers end
}, 1100);




/************ WEBSOCKET ************/


// variables
var id = 0;
var clients = {};

var currentLineNumber = 0;
var gcodeQueue = [];
var counter;
var errorCounter = 0;
var lastSent = "";
var streamLoggerFilename = "";

var printer_printingStatus = false;
var printer_printingFile = '-';
var grbl_approxTime = 0;
var grbl_timePassed = 0;
var grbl_timeLeft = 0;
var grbl_progress = 0;




// initialization for Websocket server
const wss = new WebSocket.Server({ server });

/*  TYPES  */
/*
* FROM CLIENT:  print-file - gcodeLine - reset - change-comport - list-comports - custom-command
* TO CLIENT:    printingStatus - machineStatus - myConsole - settings - console-response
*               alerts: (success - info - error - warning)
*/

// Handler for Websockets
wss.on('connection', (websocket, req) => {

  // Define Handlers
  websocket.on('message', onWebsocketMessage);
  websocket.on('close', onWebsocketClose);
  websocket.on('error', onWebsocketError);

  // id for websocket connection
	websocket.id = id++;

  // get username from url
  websocket.username = url.parse(req.url, true).query.username;

  // store websocket in clients array
  clients[websocket.id] = websocket;
  log('Connection from: "' + websocket.username + '" with ID: ' + websocket.id);
  sendToClient("myConsole", "Connected, Welcome " + websocket.username + "!" , "green");



  /******* What happens after user was stored *******/

	// Prints current status to connected clients

  // checks if GRBL is connected to server
  if (!GRBL_serialport.isOpen)
    errorAlert('GRBL is not connected to the Server, check the Serialport!')
  else
    sendToClient("myConsole", "All Services are Up and Running", "green");

  // checks if LCD Arduino is connected to server
  if (!LCD_serialport.isOpen)
    warningAlert('LCD Arduino cannot be reached!');


  // refresh PrinterStatus every 300ms
	setInterval(() => {
    if (clients[websocket.id])
      websocket.send(getPrinterStatus());
  }, 300);

	/******* Functions for Handling Requeste *******/

	// On Message Handler
	function onWebsocketMessage(data) {

    // string -> JSON
		var data = JSON.parse(data);

		try {

			if (data.type == 'gcodeLine') {
        line = data.message + "\r";
        // calls Write function
        writeToGRBL(line, (response) => {
          // logs error if there is one
          if (!response.success) return log(response.message);
          // if succeeded
          sendToClient('myConsole', "You sent: "+ line, 'black');
          sendToAllClients('myConsole', websocket.username + " sent: " + line, 'black');
          log(websocket.username + ' sent "' + line + '"');
        });

			} else if (data.type == 'reset') {
        resetAllGRBLVarialbes();
        line =  "\030";
        writeToGRBL(line, (response) => {
          // logs error if there is one
          if (!response.success) return log(response.message);
          // if succeeded
          sendToClient('myConsole', "You have reseted the Machine", 'black');
          sendToAllClients('myConsole', websocket.username + " has reseted the Machine", 'black');
          log(websocket.username + ' made a Reset');
        });

      } else if (data.type == 'print-file') {
        if (!GRBL_serialport.isOpen)
          return errorAlert("GRBL Arduino is not connected to the server");

        if (printer_printingStatus)
          return errorAlert("Printer is already printing '" + printer_printingFile + "'");
        // Analyze the file
        GRBL.GRBLAnalyzer(data.filename, (response) => {
          // logs error if there is one
          if (!response.success) return log(response.message);
          else {
            sendToClient('myConsole', response.message, 'black');
            sendToAllClients('myConsole', "From " + websocket.username + " :: " + response.message, 'black');
          }

          if (response.success && response.gcode) {
            printer_printingStatus = true;
            printer_printingFile = response.filename;
            streamLoggerFilename = response.filename + "_" + getMyDate() + ".log";
            grbl_approxTime = response.approxTime;
            gcodeQueue = response.gcode;
            currentLineNumber = -1;
            startTimePassed();
            gcodeStreamer();
            //writeRedLED(true);  // red LED ON
          }
        });

      } else if (data.type == "list-comports") {
        listSerialInfo((response) => {
          // logs error if there is one
          if (!response.success) return log(response.message);
          // if succeeded
          log(websocket.username + " requested settings");
          clients[websocket.id].send(JSON.stringify({ 'type': 'comports', message: response }));
        });

      } else if (data.type == "change-comport") {
        serialportChange(data.name, data.comPort, (response) => {
          // logs error if there is one
          if (!response.success) return log(response.message);
          // if succeeded
          infoAlert(response.message);
          sendToAllClients('myConsole', response.message, 'green');
          log(response.message);
        });

      } else if (data.type == "custom-command") {
        sendToClient("console-response", "Trying to execute: " + data.command , "green");
        exec(data.command, (error, stdout) => {
          if (error) return sendToClient("console-response", error.toString() , "red");
          return sendToClient("console-response", stdout , "black");
        });

      }
		} catch (exception) {
			errorAlert(exception);
			log('Websocket Excetion: ' + exception);
		}

	}

	// On Close Handler
	function onWebsocketClose() {
		log("onWsClose: Client disconnected");
		delete clients[websocket.id];
	}

	//On Error Handler
	function onWebsocketError(){ log("onWsError: Error"); }

	// Alert functions for frontend
	function successAlert(msg) { websocket.send( JSON.stringify({ 'type': 'success', 'message': 'Server | ' + msg }) ); }
	function infoAlert(msg) { websocket.send( JSON.stringify({ 'type': 'info', 'message': 'Server | ' + msg }) ); }
	function errorAlert(msg) { websocket.send( JSON.stringify({ 'type': 'error', 'message': 'Server | ' + msg }) ); }
	function warningAlert(msg) { websocket.send( JSON.stringify({ 'type': 'warning', 'message': 'Server | ' + msg }) ); }


  // Gives a Respone to specific User
  function sendToClient(type, data, color) {
    clients[websocket.id].send(JSON.stringify({ 'type': type, 'message': { message: data, color: color } }));
  }

  // Sends Message to all Clients
  function sendToAllClients(type, data, color) {
    wss.clients.forEach(function each(client) {
      if (client !== websocket)
        client.send(JSON.stringify({ 'type': type, 'message': { message: data, color: color } }));
    });
  }

}); // end -> wss.on('connection')


// Serialport Handler
function writeToGRBL(line, callback) {

  if (!GRBL_serialport.isOpen)
    return callback({ success: false, message: "SerialPort is not open!" });

  GRBL_serialport.write(line);
  lastSent = line;

  return callback({ success: true });
}


// GCODE Streamer
function gcodeStreamer() {

  // nothing in gcodeQueue
  if (gcodeQueue.length < 1) return;

  currentLineNumber++

  // If currentLineNumber exceed Queue -> printer finished
  if (currentLineNumber == gcodeQueue.length) {
    var hours = Math.floor(grbl_timePassed / 3600);
    grbl_timePassed -= hours * 3600;

    var minutes = Math.floor(grbl_timePassed / 60);
    grbl_timePassed -= minutes * 60;

    var seconds = grbl_timePassed % 60;

    time = hours + ':' + (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds < 10 ? '0' + seconds : seconds);

    SendToAllClients('success', 'Finished "' + printer_printingFile + '" in ' + time + ' and with ' + errorCounter + " Errors!");
    SendToAllClients('info', 'The logfile ' + streamLoggerFilename + ' will be listed in "Settings"');
    //writeRedLED(false);  // red LED OFF
    return resetAllGRBLVarialbes();
  }

  // Checks if there is still something to stream
  if (gcodeQueue.length > currentLineNumber) {
    writeToGRBL(gcodeQueue[currentLineNumber], (response) => {
      SendToAllClients('myConsole', { message: 'Streaming: ' + gcodeQueue[currentLineNumber], color: "black" });
      if (!response.success) {
        SendToAllClients('error', response.message);
        return resetAllGRBLVarialbes();
      }
    });
  }

}


// Function to return all variables in JSON for websocket
function getPrinterStatus() {
  // Create progress bar with (currentLine) / (Lines left)
  if (gcodeQueue.length > 0 && currentLineNumber > 0)
    grbl_progress = Math.round((currentLineNumber / gcodeQueue.length) * 100).toFixed(0);
  else
    grbl_progress = 0;

  // If process takes longer than expected -> no negative timeLeft
  if (grbl_approxTime >= grbl_timePassed)
    grbl_timeLeft = Math.round(grbl_approxTime - grbl_timePassed).toFixed(2);
  else
    grbl_timeLeft = 0.00;

  return JSON.stringify({
      'type':'printingStatus',
      'printer_printingStatus': printer_printingStatus,
      'printer_printingFile': 	printer_printingFile,
      'grbl_approxTime': 			  Math.round(grbl_approxTime).toFixed(2),
      'grbl_timePassed': 			  Math.round(grbl_timePassed).toFixed(2),
      'grbl_timeLeft': 				  grbl_timeLeft,
      'grbl_progress': 				  grbl_progress
  });
}


// Function to start timer for grbl_timePassed
function startTimePassed() {
  counter = setInterval(() => {
    grbl_timePassed++;
  }, 1000)
}


// Function to reset all variables
function resetAllGRBLVarialbes() {
	printer_printingStatus = false;
	printer_printingFile = '-';
	grbl_approxTime = 0;
	grbl_timePassed = 0;
	grbl_timeLeft = 0;
	grbl_progress = 0;

  currentLineNumber = 0;
  gcodeQueue = [];
  lastSent = "";
  streamLoggerFilename = "";
  errorCounter = 0;
	clearInterval(counter);
}


// Function to log all streamed commands + response
function streamLogger(data) {

  // command + '\n' + GRBL respone + '\n' for next command
  data = lastSent + data + "\n";

  // Append to file
  fs.appendFile('./logfiles/' + streamLoggerFilename, data + '\n', (error) => {
    if (error) log(error);
  });

}




/************ SERIALPORT FUNCTIONS ************/


// global variables
var GRBL_serialport;
var LCD_serialport;
var parser;

var statusInterval;

// initialization for SerialPort
function serialportInit(name, port, callback) {

  try {

    if (name == "grbl") {

      // init serial connection
      GRBL_serialport = new SerialPort(port, { baudRate: settings.GRBL_serialBaudRate, autoOpen: true }, (error) => {
        if (error) return log(error);

        callback({ success: true, message: 'GRBL connected to ' + GRBL_serialport.path + '!' });

        // Initialization for parser
        parser = new SerialPort.parsers.Readline();
        GRBL_serialport.pipe(parser);
        // setEncoding from Buffer to String
        GRBL_serialport.setEncoding('utf8');

        // Data Event hanlder
        parser.on('data', (data) => {
          SerialData.handleData(data, (error, response) => {
            if (error) return log(error);

            if (!response)
              log("GRBL_serialport: Response was empty!")
            else {
              // Needed for StreamLogger
              if ((response.status == "ok" || response.status == "error") && gcodeQueue.length > 0 && printer_printingStatus) {
                if (response.status == "ok")
                  streamLogger(response.status);
                else
                  streamLogger(response.data.message);

                gcodeStreamer();
              }

              // Needed for errorCounter
              if (response.status == "error" && printer_printingStatus)
                errorCounter++;

              // send data to all clients
              SendToAllClients(response.type, response.data);

              // if machineStatus was revieved -> send to LCD Arduino
              if (response.type == "machineStatus") {
                printToLCD(response.data);
              }

            }
          });
        });

        // Close Event handler
        GRBL_serialport.on('close', (error) => {
          if (error) {
            log('GRBL serialport | Close: ' + error);
            SendToAllClients('warning', 'GRBL SerialPort | ' + error + ' -> Please reconnect in "Settings" Page');
          }
        });

        // Error Event handler
        GRBL_serialport.on('error', (error) => {
          if (error) {
            log('GRBL serialport | Error: ', error);
            SendToAllClients('error', ' GRBL SerialPort | ' + error);
          }
        });

        // refreshes status of GRBL
        statusInterval = setInterval(() => {
          GRBL_serialport.write("?");
        }, 200);

      });

    } else if (name == "lcd") {

      // Init of LCD Arduino
      LCD_serialport = new SerialPort(port, { baudRate: settings.LCD_serialBaudRate, autoOpen: true }, (error) => {
        if (error) return log(error);

        callback({ success: true, message: 'LCD connected to ' + LCD_serialport.path + '!' });
        //GPIO.setup(11, GPIO.DIR_HIGH); // 3. green on

        // Close Event handler
        LCD_serialport.on('close', (error) => {
          if (error) {
            log('LCD Arduino | Close: ' + error);
            SendToAllClients('warning', 'LCD Arduino | ' + error);
          }
        });

        // Error Event handler
        LCD_serialport.on('error', (error) => {
          if (error) {
            log('LCD Arduino | Error: ', error);
            SendToAllClients('error', ' LCD Arduino | ' + error);
          }
        });

      });

    }

  } catch (exception) {
    console.log("SerialPortInit Exception: " + exception);
  }
}


// Call function immediately @startup for GRBL
serialportInit('grbl', settings.GRBL_serialport, (response) => {
  if (response.success)
    log(response.message);
});

// Call function immediately @startup for LCD
serialportInit('lcd', settings.LCD_serialport, (response) => {
  if (response.success)
    log(response.message);
});


// function to change GRBL_serialport
function serialportChange(name, port, callback) {

  if (!name || name == undefined || name == null)
    return callback({ success: false, message: "No name provided!" });

  if (!port || port == undefined || port == null)
    return callback({ success: false, message: "No port provided!" });

  if (name == "grbl") {

      if (GRBL_serialport.isOpen && (GRBL_serialport.path == port))
        return callback({ success: false, message: "You are already connected to port " + GRBL_serialport.path });

      if (gcodeQueue.length > 0)
        return callback({ success: false, message: "The printer queue is not empty! Wait until print has finished!" });

      if (!GRBL_serialport.isOpen) {
        serialportInit('grbl', port, (response) => {
            return callback(response);
        });
      }

      GRBL_serialport.close((error) => {
        if (error)
          return log("Serialport closing error: " + error);

        clearInterval(statusInterval);
        serialportInit('grbl', port, (response) => {
            return callback(response);
        });
      });

  } else if (name == "lcd") {

    if (LCD_serialport.isOpen && (LCD_serialport.path == port))
      return callback({ success: false, message: "You are already connected to port " + LCD_serialport.path });

    if (!LCD_serialport.isOpen) {
      serialportInit('lcd', port, (response) => {
          return callback(response);
      });
    }

    LCD_serialport.close((error) => {
      if (error)
        return log("Serialport closing error: " + error);

      serialportInit('lcd', port, (response) => {
          return callback(response);
      });
    });
  } else {
    return callback({ success: false, message: "You can only change serialport for GRBL and/or LCD!" });
  }

}



// Function to send data to LCD Arduino
function printToLCD(data) {
  if (!LCD_serialport.isOpen && !GRBL_serialport.isOpen)
    return log("LCD serialport: Not open!");
  else
    LCD_serialport.write(data.status_machine + "S" + data.coorX + "X" + data.coorY + "Y" + data.feed_rate + "F");
}


/************ HELPER FUNCTIONS ************/


function log(message) {
	message = '[' + new Date().toLocaleString() + '] ' + message;	// Add Date+Time to message
	// Add message to File -> if success write it to console
	fs.appendFile('./logFile.log', message + '\n', (error) => {
		if (error) throw error;
		else console.log(message);
	});
}

function SendToAllClients(type, data) {
  wss.clients.forEach(function each(client) {
    client.send( JSON.stringify({ 'type': type, 'message': data }) );
  });
}


function listSerialInfo(callback) {
  SerialPort.list((error, ports) => {
    if (error)
      return callback({ success: false, message: error });

    if (GRBL_serialport.isOpen)
      connectedPortGRBL = GRBL_serialport.path;
    else
      connectedPortGRBL = "Disconnected";

    if (LCD_serialport.isOpen)
      connectedPortLCD = LCD_serialport.path;
    else
      connectedPortLCD = "Disconnected"

    return callback({ success: true, ports: ports, connectedPortGRBL: connectedPortGRBL, connectedPortLCD:  connectedPortLCD });
  });
}


// function to generate DATE we can use for filename
function getMyDate() {
  myDate = new Date;
  return myDate.getDate() + "_" + (myDate.getMonth() + 1) + "_" + myDate.getFullYear() + "-" + myDate.getHours() + "_" + myDate.getMinutes() + "_" + myDate.getSeconds();
}
