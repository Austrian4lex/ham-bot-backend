# HAM-Bot Backend

This is the Backend Server for the graduation work "HAM-Bot".

## Requirements
An installed "Node.js" version is needed to start up the server (Tested on Node Version v6.11.0).
A working "MongoDB" version is also required (Tested with MongoDB Version 3.2.10).

## Installation
To install this dependency simply run: `npm install`.
All packages and needed software will be installed. (Packages listed in 'package.json' file)

## How to start server
To start up the server, simply type `node server.js` in de cloned repository.
(You can also use `nodemon server.js` (if installed))

Navigate to `http://127.0.0.1` to open up the website
Sidenote: You need to create one folders named 'files' and one 'logfiles', otherwise the server won't start properly.

## Frontend
The Frontend of this project can be found here: [Frontend](https://bitbucket.org/Austrian4lex/ham-bot-frontend)

## Further help/questions
Contact me via email: alexander.kainzinger@gmail.com
