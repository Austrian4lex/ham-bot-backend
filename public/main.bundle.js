webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_settings_settings_component__ = __webpack_require__("../../../../../src/app/components/settings/settings.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_userdb_userdb_component__ = __webpack_require__("../../../../../src/app/components/userdb/userdb.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_not_found_not_found_component__ = __webpack_require__("../../../../../src/app/components/not-found/not-found.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_auth_guard_service__ = __webpack_require__("../../../../../src/app/services/auth-guard.service.ts");
/************ DEFINE PACKAGE ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// Stuff we need


// Components




// Authguad

var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__components_dashboard_dashboard_component__["a" /* DashboardComponent */], data: { title: "Dashboard" } },
    { path: 'settings', component: __WEBPACK_IMPORTED_MODULE_3__components_settings_settings_component__["a" /* SettingsComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_6__services_auth_guard_service__["a" /* AuthGuardService */]], data: { title: "Settings" } },
    { path: 'userdb', component: __WEBPACK_IMPORTED_MODULE_4__components_userdb_userdb_component__["b" /* UserdbComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_6__services_auth_guard_service__["a" /* AuthGuardService */]], data: { title: "UserDB" } },
    { path: '404', component: __WEBPACK_IMPORTED_MODULE_5__components_not_found_not_found_component__["a" /* NotFoundComponent */], data: { title: "404" } },
    { path: '**', redirectTo: '/404' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forRoot(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "body {\r\n  padding-bottom: 65px;\r\n}\r\n\r\n.custom-dropdown {\r\n  padding: 15px 10px 15px 10px;\r\n}\r\n\r\n@media screen and (min-width:768px) {\r\n  .custom-dropdown {\r\n    width: 230px;\r\n  }\r\n}\r\n\r\n.position {\r\n  text-align: center;\r\n  margin: 4px 0 4px 0;\r\n  font-size: 25px;\r\n  width: 100%;\r\n  border: 1px solid #aaa;\r\n  font-weight: bold;\r\n  border-radius: .15rem;\r\n}\r\n\r\n.console {\r\n  background-color: #e6e6e6;\r\n  border-radius: 4px;\r\n  height: 300px;\r\n  width: 100%;\r\n  overflow: auto;\r\n  text-align: left;\r\n  padding: 10px;\r\n  margin-bottom: 10px;\r\n}\r\n\r\n.console p {\r\n  margin: 5px !important;\r\n}\r\n\r\n.webcam {\r\n  margin-bottom: 1em;\r\n}\r\n\r\n.command {\r\n  height: 450px;\r\n  width: 100%;\r\n  overflow: auto;\r\n}\r\n\r\n.gcode-files {\r\n  height: 257px;\r\n  overflow-y: auto;\r\n  overflow-x: hidden;\r\n  margin-bottom: 10px;\r\n}\r\n\r\n.gcode-file-item {\r\n  padding: 5px;\r\n  border-bottom: 1px solid #ddd;\r\n}\r\n\r\n.pointer {\r\n  cursor: pointer;\r\n}\r\n\r\n@media (min-width: 1200px) {\r\n  .container {\r\n  \tmax-width: 1450px;\r\n  }\r\n}\r\n\r\n.tabcontent {\r\n  display: none;\r\n  -webkit-animation: fadeEffect .5s;\r\n  animation: fadeEffect .5s;\r\n}\r\n\r\n.mobile-only {\r\n  display: block;\r\n}\r\n\r\n@media screen and (min-width:768px) {\r\n  .tabcontent {\r\n    display: block;\r\n    -webkit-animation: none;\r\n    animation: none;\r\n  }\r\n  .mobile-only {\r\n    display: none;\r\n  }\r\n}\r\n\r\n@-webkit-keyframes fadeEffect {\r\n  from { opacity: 0; }\r\n  to { opacity: 1; }\r\n}\r\n\r\n@keyframes fadeEffect {\r\n  from { opacity: 0; }\r\n  to { opacity: 1; }\r\n}\r\n\r\nbody::-webkit-scrollbar {\r\n    width: 8px;\r\n    height: 8px;\r\n}\r\n\r\n::-webkit-scrollbar {\r\n  width: 5px;\r\n  height: 5px;\r\n}\r\n\r\n::-webkit-scrollbar-track {\r\n  border-radius: 2px;\r\n  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);\r\n}\r\n\r\n::-webkit-scrollbar-thumb {\r\n  border-radius: 2px;\r\n  background-color: darkgrey;\r\n  outline: 1px solid slategrey;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"container\">\n  <ngbd-alert></ngbd-alert>\n  <router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_filter__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/filter.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_mergeMap__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/mergeMap.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/************ DEFINE PACKAGE AND STUFF ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Stuff we need



// Angular



var AppComponent = /** @class */ (function () {
    function AppComponent(router, activatedRoute, titleService) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.titleService = titleService;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events
            .filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* NavigationEnd */]; })
            .map(function () { return _this.activatedRoute; })
            .map(function (route) {
            while (route.firstChild)
                route = route.firstChild;
            return route;
        })
            .filter(function (route) { return route.outlet === 'primary'; })
            .mergeMap(function (route) { return route.data; })
            .subscribe(function (event) { return _this.titleService.setTitle("HAM-Bot | " + event['title']); });
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["n" /* Component */])({
            selector: 'app',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["b" /* Title */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_navbar_navbar_component__ = __webpack_require__("../../../../../src/app/components/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_settings_settings_component__ = __webpack_require__("../../../../../src/app/components/settings/settings.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_userdb_userdb_component__ = __webpack_require__("../../../../../src/app/components/userdb/userdb.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_not_found_not_found_component__ = __webpack_require__("../../../../../src/app/components/not-found/not-found.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_alert_alert_component__ = __webpack_require__("../../../../../src/app/components/alert/alert.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pipes_format_time_pipe__ = __webpack_require__("../../../../../src/app/pipes/format-time.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pipes_searchfilter_pipe__ = __webpack_require__("../../../../../src/app/pipes/searchfilter.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pipes_newtoold_pipe__ = __webpack_require__("../../../../../src/app/pipes/newtoold.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__app_routing_module__ = __webpack_require__("../../../../../src/app/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_userauth_service__ = __webpack_require__("../../../../../src/app/services/userauth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_api_request_service__ = __webpack_require__("../../../../../src/app/services/api-request.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_websocket_service__ = __webpack_require__("../../../../../src/app/services/websocket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__services_alertservice_service__ = __webpack_require__("../../../../../src/app/services/alertservice.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_auth_guard_service__ = __webpack_require__("../../../../../src/app/services/auth-guard.service.ts");
/************ DEFINE PACKAGE ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// Angular Core and import of ng-bootstrap





// Components







// Custom Pipes



// Services






var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */], __WEBPACK_IMPORTED_MODULE_7__components_navbar_navbar_component__["a" /* NavbarComponent */], __WEBPACK_IMPORTED_MODULE_6__components_dashboard_dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_settings_settings_component__["a" /* SettingsComponent */], __WEBPACK_IMPORTED_MODULE_9__components_userdb_userdb_component__["b" /* UserdbComponent */], __WEBPACK_IMPORTED_MODULE_10__components_not_found_not_found_component__["a" /* NotFoundComponent */],
                __WEBPACK_IMPORTED_MODULE_6__components_dashboard_dashboard_component__["b" /* GcodeEditor */], __WEBPACK_IMPORTED_MODULE_6__components_dashboard_dashboard_component__["d" /* PrintFile */], __WEBPACK_IMPORTED_MODULE_6__components_dashboard_dashboard_component__["c" /* MultiplePrintFile */], __WEBPACK_IMPORTED_MODULE_9__components_userdb_userdb_component__["a" /* UserEditor */], __WEBPACK_IMPORTED_MODULE_11__components_alert_alert_component__["a" /* NgbdAlert */],
                __WEBPACK_IMPORTED_MODULE_12__pipes_format_time_pipe__["a" /* FormatTimePipe */], __WEBPACK_IMPORTED_MODULE_13__pipes_searchfilter_pipe__["a" /* SearchFilterPipe */], __WEBPACK_IMPORTED_MODULE_14__pipes_newtoold_pipe__["a" /* NewtooldPipe */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_15__app_routing_module__["a" /* AppRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__["c" /* NgbModule */].forRoot()
            ],
            entryComponents: [__WEBPACK_IMPORTED_MODULE_9__components_userdb_userdb_component__["a" /* UserEditor */], __WEBPACK_IMPORTED_MODULE_6__components_dashboard_dashboard_component__["b" /* GcodeEditor */], __WEBPACK_IMPORTED_MODULE_6__components_dashboard_dashboard_component__["d" /* PrintFile */], __WEBPACK_IMPORTED_MODULE_6__components_dashboard_dashboard_component__["c" /* MultiplePrintFile */]],
            providers: [__WEBPACK_IMPORTED_MODULE_16__services_userauth_service__["a" /* UserauthService */], __WEBPACK_IMPORTED_MODULE_17__services_api_request_service__["a" /* ApiRequestService */], __WEBPACK_IMPORTED_MODULE_19__services_alertservice_service__["a" /* AlertService */], __WEBPACK_IMPORTED_MODULE_18__services_websocket_service__["a" /* WebsocketService */], __WEBPACK_IMPORTED_MODULE_20__services_auth_guard_service__["a" /* AuthGuardService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/components/alert/alert.component.html":
/***/ (function(module, exports) {

module.exports = "<p *ngFor=\"let alert of alerts\">\n  <ngb-alert [type]=\"alert.type\" (close)=\"closeAlert(alert)\">{{ alert.message }}</ngb-alert>\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/alert/alert.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NgbdAlert; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_alertservice_service__ = __webpack_require__("../../../../../src/app/services/alertservice.service.ts");
/************ DEFINE PACKAGE AND STUFF ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Stuff we need

// Service Imports

/************ ALERT COMPONENT ************/
var NgbdAlert = /** @class */ (function () {
    function NgbdAlert(alertService) {
        var _this = this;
        this.alertService = alertService;
        this.alerts = [];
        this.subscription = this.alertService.getAlerts()
            .subscribe(function (Alerts) { _this.alerts.push(Alerts); });
    }
    NgbdAlert.prototype.ngOnDestroy = function () {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
    };
    NgbdAlert.prototype.closeAlert = function (alert) {
        var index = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    };
    NgbdAlert = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ngbd-alert',
            template: __webpack_require__("../../../../../src/app/components/alert/alert.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_alertservice_service__["a" /* AlertService */]])
    ], NgbdAlert);
    return NgbdAlert;
}());



/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "body {\r\n  padding-bottom: 65px;\r\n}\r\n\r\n.console-clearbutton {\r\n  float: right;\r\n}\r\n\r\n.main-console{\r\n  background-color: #e6e6e6;\r\n  border-radius: .25rem;\r\n}\r\n\r\n.console-options {\r\n  margin: 0;\r\n  padding: 4px;\r\n  height: 40px;\r\n  border-top: 2px solid rgba(0, 0, 0, 0.125);\r\n}\r\n\r\n.custom-button {\r\n  width: 100%;\r\n}\r\n\r\n.position {\r\n  text-align: center;\r\n  margin: 4px 0 4px 0;\r\n  font-size: 25px;\r\n  width: 100%;\r\n  border: 1px solid #aaa;\r\n  font-weight: bold;\r\n  border-radius: .15rem;\r\n}\r\n\r\n.console {\r\n  background-color: #e6e6e6;\r\n  border-radius: 4px;\r\n  height: 362px;\r\n  width: 100%;\r\n  overflow: auto;\r\n  text-align: left;\r\n  padding-left: 15px;\r\n  padding: 10px;\r\n  margin-bottom: 10px;\r\n}\r\n\r\n.console p {\r\n  margin: 7px !important;\r\n}\r\n\r\n.webcam {\r\n  margin-bottom: 1em;\r\n}\r\n\r\n.gcode-files {\r\n  height: 319px;\r\n  overflow-y: auto;\r\n  overflow-x: hidden;\r\n  margin-bottom: 10px;\r\n}\r\n\r\n.gcode-file-item {\r\n  padding: 5px;\r\n  border-bottom: 1px solid #ddd;\r\n}\r\n\r\n.pointer {\r\n  cursor: pointer;\r\n}\r\n\r\n.tabcontent {\r\n  -webkit-animation: fadeEffect .5s;\r\n  animation: fadeEffect .5s;\r\n  margin-bottom: 1em\r\n}\r\n\r\n.mobile-only {\r\n  display: block;\r\n  padding: 2px !important;\r\n}\r\n\r\n@media screen and (max-width:768px) {\r\n  .tabcontent {\r\n      margin-bottom: 4em;\r\n  }\r\n}\r\n\r\n@media screen and (min-width:768px) {\r\n  .tabcontent {\r\n    display: block;\r\n    -webkit-animation: none;\r\n    animation: none;\r\n\r\n  }\r\n  .mobile-only {\r\n    display: none;\r\n  }\r\n}\r\n\r\n@-webkit-keyframes fadeEffect {\r\n  from { opacity: 0; }\r\n  to { opacity: 1; }\r\n}\r\n\r\n@keyframes fadeEffect {\r\n  from { opacity: 0; }\r\n  to { opacity: 1; }\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <!-- Status tab -->\n  <div class=\"col-lg-3 tabcontent\" *ngIf=\"tabStatus\">\n    <div class=\"card\">\n      <h4 class=\"card-header\">Status</h4>\n      <div class=\"card-body\">\n        <h5 class=\"card-title\">Status: {{ status_machine }}\n          <ng-template #machineState>\n            'Hold:0' Hold complete. Ready to resume. <br>\n            'Hold:1' Hold in-progress. Reset will throw an alarm. <br>\n            'Door:0' Door closed. Ready to resume. <br>\n            'Door:1' Machine stopped. Door still ajar. Can't resume until closed. <br>\n            'Door:2' Door opened. Hold (or parking retract) in-progress. Reset will throw an alarm. <br>\n            'Door:3' Door closed and resuming. Restoring from park, if applicable. Reset will throw an alarm.\n          </ng-template>\n          <span class=\"pointer\" [ngbPopover]=\"machineState\" popoverTitle=\"Machine State\" placement=\"bottom\" triggers=\"mouseenter:mouseleave\"><i class=\"fas fa-info-circle\"></i></span>\n        </h5>\n        <hr>\n        <div class=\"row\">\n          <div class=\"col col-xl\">\n            <strong>Coordinates</strong>\n            <div class=\"position\">X: {{ coorX }}</div>\n            <div class=\"position\">Y: {{ coorY }}</div>\n            <strong>Feedrate</strong>\n            <div class=\"position\">F: {{ feed_rate }}</div>\n          </div>\n        </div>\n        <hr>\n\n          File:          <strong>{{ printer_printingFile }}</strong><br/>\n          Approx. Time:  <strong>{{ grbl_approxTime | formatTime }}</strong><br/>\n          Time Passed:   <strong>{{ grbl_timePassed| formatTime  }}</strong><br/>\n          Time Left:     <strong>{{ grbl_timeLeft| formatTime  }}</strong>\n\n        <hr>\n\n        <div class=\"text-center\">Progress: {{ grbl_progress }}%</div>\n        <div class=\"progress\">\n          <div class=\"progress-bar\" role=\"progressbar\" [ngStyle]=\"{'width': grbl_progress + '%'}\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n        </div>\n\n        <hr>\n\n        <!-- Stepsize -->\n        <div class=\"row\" style=\"margin-bottom: 1em;\">\n          <div class=\"col\">\n            <strong>Step Size: </strong>\n          </div>\n          <div class=\"col-8\">\n            <select class=\"form-control form-control-sm\" ngControl=\"type\" #stepsizeinput>\n              <option *ngFor=\"let stepsize of stepsizes\" [value]=\"stepsize\" >{{ stepsize }}</option>\n            </select>\n          </div>\n        </div>\n\n        <!-- Buttons X+ X- Y+ Y- -->\n        <div class=\"row\" style=\"margin-bottom: .5em;\">\n          <div class=\"col\"></div>\n          <div class=\"col\"><button type=\"button\" class=\"btn btn-primary custom-button\" (click)=\"sendGcode('G91 G0 Y'+ stepsizeinput.value)\" title=\"Move the current position to Y+ with the selected step size\" [disabled]=\"!user\">Y+</button></div>\n          <div class=\"col\"></div>\n        </div>\n        <div class=\"row\" style=\"margin-bottom: .5em;\">\n          <div class=\"col-2\"></div>\n          <div class=\"col-4\">\n            <button type=\"button\" class=\"btn btn-primary custom-button\" (click)=\"sendGcode('G91 G0 X-'+ stepsizeinput.value)\" title=\"Move the current position to X- with the selected step size\" [disabled]=\"!user\">X-</button>\n          </div>\n          <div class=\"col-4\">\n            <button type=\"button\" class=\"btn btn-primary custom-button\" (click)=\"sendGcode('G91 G0 X'+ stepsizeinput.value)\" title=\"Move the current position to X+ with the selected step size\" [disabled]=\"!user\">X+</button>\n          </div>\n          <div class=\"col-2\"></div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col\"></div>\n          <div class=\"col\"><button type=\"button\" class=\"btn btn-primary custom-button\" (click)=\"sendGcode('G91 G0 Y-'+ stepsizeinput.value)\" title=\"Move the current position to Y- with the selected step size\" [disabled]=\"!user\">Y-</button></div>\n          <div class=\"col\"></div>\n        </div>\n        <hr>\n        <div class=\"row\" >\n          <div class=\"col\">\n            <button type=\"button\" class=\"btn btn-primary btn-block\" (click)=\"sendGcode('M3 S0')\" [disabled]=\"!user\">Pen Up</button>\n          </div>\n          <div class=\"col\">\n            <button type=\"button\" class=\"btn btn-primary btn-block\" (click)=\"sendGcode('M3 S255')\" [disabled]=\"!user\">Pen Down</button>\n          </div>\n        </div>\n\n      </div>\n    </div>\n  </div> <!-- col-lg-3 end -->\n\n\n  <!-- Webcam + Console & Commandline-->\n  <div class=\"col-lg-9\">\n    <div class=\"row\">\n\n      <!-- Webcam + Console-->\n      <div class=\"col-xl-8 tabcontent\" *ngIf=\"tabWebcam_Console\">\n        <!-- Webcam -->\n        <div class=\"webcam\" id=\"webcam-tab\">\n          <div class=\"embed-responsive embed-responsive-16by9\">\n            <img class=\"embed-responsive-item\" src=\"{{ streamAdress }}\">\n          </div>\n        </div>\n        <!-- Console -->\n        <div class=\"main-console\">\n          <pre class=\"console\">\n              <p *ngFor=\"let message of messages\"><span [ngStyle]=\"{'color': message.color}\">{{ message.message }}</span></p>\n          </pre>\n          <div class=\"console-options\">\n            <button type=\"button\" class=\"btn btn-primary btn-sm console-clearbutton\" (click)=\"clearConsole()\">Clear</button>\n          </div>\n        </div>\n      </div> <!-- col-lg-8 end -->\n\n\n      <!-- Commandline -->\n      <div class=\"col-xl-4 tabcontent\" *ngIf=\"tabCommandline\">\n        <div class=\"card\">\n          <h4 class=\"card-header\">Commandline</h4>\n          <div class=\"card-body\">\n\n            <!-- Button Shortcuts @Commandline -->\n            <div class=\"row\" style=\"margin-bottom: 1em;\">\n              <div class=\"col\">\n                 <button type=\"button\" class=\"btn btn-primary btn-block\" (click)=\"sendGcode('$')\" title=\"Send 'Help' to GRBL to get a short helpmessage\" [disabled]=\"!user\">Help ($)</button>\n              </div>\n              <div class=\"col\">\n                <button type=\"button\" class=\"btn btn-primary btn-block\" (click)=\"sendGcode('$$')\" title=\"Get a response of current settings\" [disabled]=\"!user\">Settings ($$)</button>\n              </div>\n              <div class=\"w-100\" style=\"margin-bottom: 1em;\"></div>\n              <div class=\"col\">\n                <button type=\"button\" class=\"btn btn-primary btn-block\" (click)=\"sendGcode('?')\" title=\"Get a response of current status\" [disabled]=\"!user\">Status (?)</button>\n              </div>\n              <div class=\"col\">\n                <button type=\"button\" class=\"btn btn-primary btn-block\" (click)=\"softReset()\" title=\"Make a soft reset on GRBL\" [disabled]=\"!user\">Soft Reset</button>\n              </div>\n              <div class=\"w-100\" style=\"margin-bottom: 1em;\"></div>\n              <div class=\"col\">\n                <button type=\"button\" class=\"btn btn-primary btn-block\" (click)=\"sendGcode('!')\" title=\"Stop the machine\" [disabled]=\"!user\">Feed Hold (!)</button>\n              </div>\n              <div class=\"col\">\n                <button type=\"button\" class=\"btn btn-primary btn-block\" (click)=\"sendGcode('~')\" title=\"Resume the machine\" [disabled]=\"!user\">Resume (~)</button>\n              </div>\n            </div>\n\n\n            <!-- Input @Commandline for GRBL -->\n            <div class=\"input-group mb-3\">\n              <div class=\"input-group-prepend\">\n                <span class=\"input-group-text btn\"  data-toggle=\"modal\" data-target=\"#availableCommands\"><i class=\"fas fa-terminal\"></i></span>\n              </div>\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"gcodeInput\" #line (keyup.enter)=\"sendGcode(line.value)\" placeholder=\"Command\" [disabled]=\"!user\">\n              <div class=\"input-group-append\">\n                <span class=\"input-group-text btn\" (click)=\"sendGcode(line.value)\">Send</span>\n              </div>\n            </div>\n\n            <!-- Files Area -->\n            <div class=\"card\">\n              <div class=\"card-header\"><b>Files</b></div>\n              <div style=\"padding: 10px;\">\n                <!-- Searchbox -->\n                <div class=\"form-group\">\n                  <div class=\"input-group\">\n                    <input type=\"text\" class=\"form-control form-control-sm\" placeholder=\"Search...\" #searchInput>\n                  </div>\n                </div>\n                <!-- Starting with those files -->\n                <div class=\"gcode-files\">\n                  <div class=\"gcode-file-item\" *ngFor=\"let file of files | searchFilter : searchInput.value\">\n                    <div class=\"text-primary\" style=\"padding-bottom: 5px;\"><b>{{ file.filename }}</b></div>\n                    <div class=\"row\">\n                      <div class=\"col text-muted\"><small>Created: {{ file.created | date: 'dd.MM.yyyy' }}</small></div>\n                      <div class=\"col-5\">\n                        <div class=\"btn-group btn-group-sm float-right\" role=\"group\" style=\"padding-bottom: 5px;\">\n                          <button type=\"button\" class=\"btn btn-secondary\" (click)=\"downloadFile(file.filename)\" title=\"Download\"><i class=\"fas fa-download\"></i></button>\n                          <button type=\"button\" class=\"btn btn-secondary\" (click)=\"deleteFile(file.filename)\" title=\"Delete\" [disabled]=\"!user\"><i class=\"fas fa-trash\"></i></button>\n                          <button type=\"button\" class=\"btn btn-secondary\" (click)=\"editFile(file.filename)\" title=\"Edit\" [disabled]=\"!user\"><i class=\"fas fa-edit\"></i></button>\n                          <button type=\"button\" class=\"btn btn-secondary\" (click)=\"printFile(file.filename)\" title=\"Print\" [disabled]=\"!user\"><i class=\"fas fa-print\"></i></button>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n\n            <br>\n\n            <!-- File Upload -->\n            <div class=\"input-group\">\n              <div class=\"input-group-prepend\">\n                <span class=\"input-group-text\"><i class=\"fas fa-upload\"></i></span>\n              </div>\n              <div class=\"custom-file\">\n                <input type=\"file\" class=\"custom-file-input pointer\" id=\"fileupload\" #inputFile (change)=\"uploadFile()\" name=\"uploads[]\" multiple=\"multiple\">\n                <label class=\"custom-file-label pointer\" for=\"fileupload\">Choose a File ...</label>\n              </div>\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n      <!-- Modal for Input @Commandline (Available Commands)-->\n      <div class=\"modal fade\" id=\"availableCommands\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"commandsLabel\" aria-hidden=\"true\">\n        <div class=\"modal-dialog\" role=\"document\">\n          <div class=\"modal-content\">\n            <div class=\"modal-header\">\n              <h4 class=\"modal-title\" id=\"commandsLabel\">Available Commands</h4>\n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\n            </div>\n            <div class=\"modal-body\">\n              <!-- $Nx=line $J=line -->\n              <p>\n                $$ and $x=val - View and write Grbl settings <br>\n                $# - View gcode parameters <br>\n                $I - View build info <br>\n                $G - View gcode parser state <br>\n                $N - View startup blocks <br>\n                $C - Check gcode mode <br>\n                $X - Kill alarm lock <br>\n                $H - Run homing cycle <br>\n                $SLP - Enable Sleep Mode\n              </p>\n            </div>\n            <!-- Yet another input for commands -->\n             <div class=\"card-footer\">\n              <div class=\"input-group\">\n                <input type=\"text\" class=\"form-control\" #line2 placeholder=\"Command\" (keyup.enter)=\"sendGcode(line2.value)\">\n                <div class=\"input-group-append\">\n                  <span class=\"input-group-text btn pointer\" (click)=\"sendGcode(line2.value)\">Send</span>\n                </div>\n              </div>\n            </div>\n\n          </div>\n        </div>\n      </div> <!-- Modal end-->\n\n    </div>  <!-- row end-->\n\n  </div> <!-- col-lg-9 end -->\n</div>  <!-- main row end -->\n\n\n<!-- Navbar for mobile users @bottom -->\n<nav class=\"navbar fixed-bottom navbar-dark bg-primary mobile-only\">\n  <ul class=\"nav nav-pills nav-justified\">\n    <li class=\"nav-item\"><a href=\"javascript:0\" (click)=\"changeTab('status')\" class=\"nav-link\"><i style=\"color:#fff;\" class=\"fas fa-cogs fa-2x\"></i></a></li>\n    <li class=\"nav-item\"><a href=\"javascript:0\" (click)=\"changeTab('webcam_console')\" class=\"nav-link\"><i style=\"color:#fff;\" class=\"fas fa-camera fa-2x\"></i></a></li>\n    <li class=\"nav-item\"><a href=\"javascript:0\" (click)=\"changeTab('commandline')\" class=\"nav-link\"><i style=\"color:#fff;\" class=\"fas fa-terminal fa-2x\"></i></a></li>\n  </ul>\n</nav>\n"

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return GcodeEditor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return PrintFile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return MultiplePrintFile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_file_saver__ = __webpack_require__("../../../../file-saver/FileSaver.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_file_saver___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_file_saver__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_request_service__ = __webpack_require__("../../../../../src/app/services/api-request.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_alertservice_service__ = __webpack_require__("../../../../../src/app/services/alertservice.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_userauth_service__ = __webpack_require__("../../../../../src/app/services/userauth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_websocket_service__ = __webpack_require__("../../../../../src/app/services/websocket.service.ts");
/************ DEFINE PACKAGE AND STUFF ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Stuff we need



// Service Imports




/************ GCODE-EDITOR COMPONENT ************/
var GcodeEditor = /** @class */ (function () {
    function GcodeEditor(activeModal, alertService, API) {
        this.activeModal = activeModal;
        this.alertService = alertService;
        this.API = API;
    }
    // Function that get's called on submit
    GcodeEditor.prototype.onEditSubmit = function (filename, newGcode) {
        var _this = this;
        if (newGcode == "")
            return this.alertService.newAlert('danger', 'Check your GCODE! It is not allowed to submit empty GCODE');
        var file = {
            filename: filename,
            gcode: newGcode
        };
        this.API.editFile(file).subscribe(function (response) {
            if (!response.success) {
                _this.alertService.newAlert('danger', response.message);
            }
            else {
                _this.activeModal.close('Close click');
                _this.alertService.newAlert('success', response.message);
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], GcodeEditor.prototype, "filename", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], GcodeEditor.prototype, "gcode", void 0);
    GcodeEditor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'gcode-editor',
            template: "\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title\">Editing {{ filename }}</h4>\n      <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n    </div>\n    <div class=\"modal-body\">\n      <form (submit)=\"onEditSubmit()\">\n        <div class=\"form-group\">\n          <label for=\"gcode\">GCODE of file</label>\n          <textarea class=\"form-control\" rows=\"20\" #newGcode>{{ gcode }}</textarea>\n        </div>\n      </form>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"submit\" class=\"btn btn-primary\" (click)=\"onEditSubmit(filename, newGcode.value)\">Send</button>\n      <button type=\"button\" class=\"btn btn-secondary\" (click)=\"activeModal.close('Close click')\">Cancel</button>\n    </div>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* NgbActiveModal */],
            __WEBPACK_IMPORTED_MODULE_4__services_alertservice_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_3__services_api_request_service__["a" /* ApiRequestService */]])
    ], GcodeEditor);
    return GcodeEditor;
}());

/************ PRINT-FILE COMPONENT (only 1 file was uploaded) ************/
var PrintFile = /** @class */ (function () {
    function PrintFile(activeModal, alertService, ws) {
        this.activeModal = activeModal;
        this.alertService = alertService;
        this.ws = ws;
    }
    // Function that get's called on submit
    PrintFile.prototype.onPrintSubmit = function (filename) {
        if (filename == "" || filename == undefined || filename == null)
            return this.alertService.newAlert('danger', 'No filename was provided!');
        this.ws.send({ type: "print-file", filename: filename });
        this.activeModal.close('Close click');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], PrintFile.prototype, "filenames", void 0);
    PrintFile = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'print-file',
            template: "\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title\">Do you want to print {{ filename }}?</h4>\n      <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"submit\" class=\"btn btn-primary\" (click)=\"onPrintSubmit(filename)\">Print</button>\n      <button type=\"button\" class=\"btn btn-secondary\" (click)=\"activeModal.close('Close click')\">Cancel</button>\n    </div>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* NgbActiveModal */],
            __WEBPACK_IMPORTED_MODULE_4__services_alertservice_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_6__services_websocket_service__["a" /* WebsocketService */]])
    ], PrintFile);
    return PrintFile;
}());

/************ PRINT-FILE COMPONENT (only multiple files were uploaded) ************/
var MultiplePrintFile = /** @class */ (function () {
    function MultiplePrintFile(activeModal, alertService, ws) {
        this.activeModal = activeModal;
        this.alertService = alertService;
        this.ws = ws;
    }
    // Function that get's called on submit
    MultiplePrintFile.prototype.onPrintSubmit = function (filename) {
        if (filename == "" || filename == undefined || filename == null)
            return this.alertService.newAlert('danger', 'No filename was provided!');
        this.ws.send({ type: "print-file", filename: filename });
        this.activeModal.close('Close click');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], MultiplePrintFile.prototype, "filename", void 0);
    MultiplePrintFile = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'multi-print-file',
            template: "\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title\">Do you want to print one of those recent uploaded files?</h4>\n      <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n    </div>\n    <div class=\"modal-body\">\n      <div class=\"form-group row\" *ngFor=\"let file of filenames\">\n        <label for=\"file\" class=\"col-sm col-form-label\">Filename: {{ file }}</label>\n        <div class=\"col-sm\">\n          <button type=\"submit\" class=\"btn btn-primary\" (click)=\"onPrintSubmit(file)\">Print</button>\n        </div>\n      </div>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" class=\"btn btn-secondary\" (click)=\"activeModal.close('Close click')\">Cancel</button>\n    </div>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* NgbActiveModal */],
            __WEBPACK_IMPORTED_MODULE_4__services_alertservice_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_6__services_websocket_service__["a" /* WebsocketService */]])
    ], MultiplePrintFile);
    return MultiplePrintFile;
}());

/************ DASHBOARD COMPONENT ************/
var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(API, alertService, ws, el, modalService, userAuthService) {
        var _this = this;
        this.API = API;
        this.alertService = alertService;
        this.ws = ws;
        this.el = el;
        this.modalService = modalService;
        this.userAuthService = userAuthService;
        // messages variable for websocket
        this.messages = [];
        // variable for user
        this.user = false;
        // variables for tabs
        this.tabStatus = true;
        this.tabWebcam_Console = true;
        this.tabCommandline = true;
        this.files = [];
        this.streamAdress = "";
        // Get Display width, lower than 768px -> mobile
        // show only status tab on load
        this.displayWidth = window.innerWidth;
        if (this.displayWidth <= 768) {
            this.tabWebcam_Console = false;
            this.tabCommandline = false;
        }
        // set available stepsizes
        this.stepsizes = ['100', '10', '1', '0.1', '0.01'];
        // websocket initialization
        this.API.getWebSocketURL().subscribe(function (response) {
            _this.ws.WebsocketConnection(response.url).subscribe(function (data) {
                _this.handleMessage(data);
            });
        });
        // refresh files
        this.refreshFiles();
        this.userAuthService.loginState().subscribe(function (response) {
            _this.user = response.username;
        });
        this.streamAdress = window.location.origin + ":8081";
    }
    // Function to handle messages from websocket
    DashboardComponent.prototype.handleMessage = function (data) {
        try {
            switch (data.type) {
                case 'printingStatus': {
                    this.grbl_approxTime = data.grbl_approxTime;
                    this.grbl_timeLeft = data.grbl_timeLeft;
                    this.grbl_timePassed = data.grbl_timePassed;
                    this.printer_printingFile = data.printer_printingFile;
                    this.grbl_progress = data.grbl_progress;
                    break;
                }
                case 'machineStatus': {
                    this.status_machine = data.message.status_machine;
                    this.coorX = data.message.coorX;
                    this.coorY = data.message.coorY;
                    this.feed_rate = data.message.feed_rate;
                    break;
                }
                case 'myConsole': {
                    if (this.messages.length >= 80)
                        this.messages.splice(0, 1);
                    if (data.message.message.length > 80) {
                        data.message.message = data.message.message.substring(0, 80) + "\n" + data.message.message.substring(80);
                    }
                    this.messages.push(data.message);
                    // Always scroll to bottom automatically
                    var scrollPane = this.el.nativeElement.querySelector('.console');
                    // check if console is active (error on mobile otherwise)
                    if (scrollPane)
                        scrollPane.scrollTop = scrollPane.scrollHeight;
                    break;
                }
                case 'success': {
                    this.alertService.newAlert('success', data.message);
                    break;
                }
                case 'info': {
                    this.alertService.newAlert('info', data.message);
                    break;
                }
                case 'error': {
                    this.alertService.newAlert('danger', data.message);
                    break;
                }
                case 'warning': {
                    this.alertService.newAlert('warning', data.message);
                    break;
                }
            }
        }
        catch (exception) {
            this.alertService.newAlert('danger', 'handleMessage: Excetion: ' + exception);
        }
    };
    // Function to clear console
    DashboardComponent.prototype.clearConsole = function () {
        this.messages = [];
    };
    // Handler for shortcut buttons
    DashboardComponent.prototype.sendGcode = function (line) {
        if (!this.user)
            return;
        if (!line || line == "" || line == undefined || line == null)
            return this.alertService.newAlert('danger', 'No line provided to send to GRBL');
        this.ws.send({ type: "gcodeLine", message: line });
        this.gcodeInput = "";
    };
    // Handler for ResetButton on Website
    DashboardComponent.prototype.softReset = function () {
        this.ws.send({ type: "reset" });
    };
    //Handler for downloading files
    DashboardComponent.prototype.downloadFile = function (filename) {
        if (!filename || filename == "" || filename == undefined || filename == null)
            return this.alertService.newAlert('danger', 'No filename was provided!');
        var FILENAME = filename;
        this.API.downloadFile(filename).subscribe(function (data) {
            // saveAs from file-saver
            Object(__WEBPACK_IMPORTED_MODULE_2_file_saver__["saveAs"])(data, FILENAME);
        });
    };
    // Function to upload file + handle response (PrintFile Question)
    DashboardComponent.prototype.uploadFile = function () {
        var _this = this;
        // input element on website -> create FormData
        var inputFile = this.inputFile.nativeElement;
        var fileCount = inputFile.files.length;
        var formData = new FormData();
        // Checks if files were provided
        if (fileCount > 0) {
            // Create formData (also multiple files)
            for (var i = 0; i < fileCount; i++) {
                formData.append('uploads[]', inputFile.files.item(i), inputFile.files.item(i).name);
            }
            // Send formData to server
            this.API.uploadFile(formData).subscribe(function (response) {
                if (response.success) {
                    _this.refreshFiles();
                    _this.alertService.newAlert('success', response.message);
                    if (response.filename.length > 1) {
                        // call Modal
                        var modalRef = _this.modalService.open(MultiplePrintFile);
                        modalRef.componentInstance.filenames = response.filename;
                    }
                    else {
                        // call Modal
                        var modalRef = _this.modalService.open(PrintFile);
                        modalRef.componentInstance.filename = response.filename;
                    }
                    _this.refreshFiles();
                }
                else
                    _this.alertService.newAlert('danger', response.message);
            });
        }
        else
            this.alertService.newAlert('danger', 'No files were provided on input');
    };
    // Handler to delete Files
    DashboardComponent.prototype.deleteFile = function (filename) {
        var _this = this;
        if (!filename || filename == "" || filename == undefined || filename == null)
            return this.alertService.newAlert('danger', 'No filename was provided!');
        this.API.deleteFile(filename).subscribe(function (response) {
            if (response.success) {
                // 2x refresh to keep it up-to-date
                _this.refreshFiles();
                _this.alertService.newAlert('success', response.message);
                _this.refreshFiles();
            }
            else {
                _this.alertService.newAlert('danger', response.message);
            }
        });
    };
    // Handler to edit files
    DashboardComponent.prototype.editFile = function (filename) {
        var _this = this;
        if (!filename || filename == "" || filename == undefined || filename == null)
            return this.alertService.newAlert('danger', 'No filename was provided!');
        this.API.textFromFile(filename).subscribe(function (response) {
            // call Modal
            var modalRef = _this.modalService.open(GcodeEditor);
            modalRef.componentInstance.filename = filename;
            modalRef.componentInstance.gcode = response;
        });
    };
    // Handler to send printrequest to ws
    DashboardComponent.prototype.printFile = function (filename) {
        if (!filename || filename == "" || filename == undefined || filename == null)
            return this.alertService.newAlert('danger', 'No filename was provided!');
        this.ws.send({ type: "print-file", filename: filename });
    };
    // Handler to refresh files
    DashboardComponent.prototype.refreshFiles = function () {
        var _this = this;
        this.API.getAllFiles().subscribe(function (response) {
            if (response.success)
                _this.files = response.message;
        });
    };
    // Function to change tabs on mobile
    DashboardComponent.prototype.changeTab = function (tabname) {
        this.tabStatus = false;
        this.tabWebcam_Console = false;
        this.tabCommandline = false;
        switch (tabname) {
            case 'status':
                this.tabStatus = true;
                break;
            case 'webcam_console':
                this.tabWebcam_Console = true;
                break;
            case 'commandline':
                this.tabCommandline = true;
                break;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('inputFile'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], DashboardComponent.prototype, "inputFile", void 0);
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-dashboard',
            template: __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_api_request_service__["a" /* ApiRequestService */],
            __WEBPACK_IMPORTED_MODULE_4__services_alertservice_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_6__services_websocket_service__["a" /* WebsocketService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */],
            __WEBPACK_IMPORTED_MODULE_5__services_userauth_service__["a" /* UserauthService */]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media (min-width: 1200px) {\r\n  .container {\r\n  \tmax-width: 1450px;\r\n  }\r\n}\r\n\r\n.customFont {\r\n  color: #ffffff !important;\r\n  font-size: 18px;\r\n  font-weight: 600;\r\n}\r\n\r\n.tabcontent {\r\n  display: none;\r\n  -webkit-animation: fadeEffect .5s;\r\n  animation: fadeEffect .5s;\r\n}\r\n\r\n.mobile-only {\r\n  display: block;\r\n}\r\n\r\n@media screen and (min-width:768px) {\r\n  .tabcontent {\r\n    display: block;\r\n    -webkit-animation: none;\r\n    animation: none;\r\n  }\r\n  .mobile-only {\r\n    display: none;\r\n  }\r\n}\r\n\r\n@-webkit-keyframes fadeEffect {\r\n  from { opacity: 0; }\r\n  to { opacity: 1; }\r\n}\r\n\r\n@keyframes fadeEffect {\r\n  from { opacity: 0; }\r\n  to { opacity: 1; }\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Navbar for not loged in user -->\n<section *ngIf=\"!user\">\n  <nav class=\"navbar navbar-expand-lg navbar-dark bg-primary\">\n    <div class=\"container\">\n      <a class=\"navbar-brand\" [routerLink]=\"['/']\">HAM-Bot</a>\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavDropdown\" aria-controls=\"navbarNavDropdown\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n        <span class=\"navbar-toggler-icon\"></span>\n      </button>\n      <div class=\"collapse navbar-collapse justify-content-end\" id=\"navbarNavDropdown\">\n        <ul class=\"navbar-nav\">\n          <form class=\"form-inline\" (submit)=\"onLoginSubmit()\">\n            <input class=\"form-control mr-sm-2\" type=\"text\" [(ngModel)]=\"username\" name=\"username\" placeholder=\"Username\">\n            <input class=\"form-control mr-sm-2\" type=\"password\" [(ngModel)]=\"password\" name=\"password\" placeholder=\"Password\">\n            <button type=\"submit\" class=\"btn btn-light\"><i class=\"fas fa-sign-in-alt\"></i> Sign In</button>\n          </form>\n        </ul>\n      </div>\n    </div>\n  </nav>\n<br>\n</section>\n\n<!-- Navbar for loged in user -->\n<section *ngIf=\"user\">\n<nav class=\"navbar navbar-expand-lg navbar-dark bg-primary\">\n  <div class=\"container\">\n    <a class=\"navbar-brand\" [routerLink]=\"['/']\">HAM-Bot</a>\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNavDropdown\" aria-controls=\"navbarNavDropdown\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n      <span class=\"navbar-toggler-icon\"></span>\n    </button>\n    <div class=\"collapse navbar-collapse justify-content-end\" id=\"navbarNavDropdown\">\n      <ul class=\"navbar-nav\">\n        <li class=\"nav-item dropdown\">\n          <a class=\"nav-link dropdown-toggle customFont\" href=\"\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n            Logged in as {{ user }}\n          </a>\n          <div class=\"dropdown-menu\" aria-labelledby=\"Dropdown with more options\">\n            <a class=\"dropdown-item\" [routerLink]=\"['/userdb']\"><i class=\"fas fa-users\"></i> UserDB</a>\n            <a class=\"dropdown-item\" [routerLink]=\"['/settings']\"><i class=\"fas fa-cogs\"></i> Settings</a>\n            <a class=\"dropdown-item\" (click)=\"logout()\" href=\"\"><i class=\"fas fa-sign-out-alt\"></i> Logout</a>\n          </div>\n        </li>\n      </ul>\n    </div>\n  </div>\n</nav>\n<br>\n<section>\n"

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_userauth_service__ = __webpack_require__("../../../../../src/app/services/userauth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_alertservice_service__ = __webpack_require__("../../../../../src/app/services/alertservice.service.ts");
/************ DEFINE PACKAGE AND STUFF ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Stuff we need

// Service Imports


/************ NAVBAR COMPONENT ************/
var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(userAuthService, alertService) {
        this.userAuthService = userAuthService;
        this.alertService = alertService;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userAuthService.loginState().subscribe(function (response) {
            _this.user = response.username;
        });
    };
    // Function that get's called on submit
    NavbarComponent.prototype.onLoginSubmit = function () {
        var _this = this;
        var user = {
            username: this.username,
            password: this.password
        };
        this.userAuthService.login(user).subscribe(function (response) {
            if (!response.success)
                _this.alertService.newAlert('danger', response.message);
            else {
                window.location.reload();
            }
        });
    };
    // Function for logout requests
    NavbarComponent.prototype.logout = function () {
        this.userAuthService.logout().subscribe(function (response) {
            if (response.success) {
                window.location.reload();
            }
        });
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-navbar',
            template: __webpack_require__("../../../../../src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_userauth_service__["a" /* UserauthService */],
            __WEBPACK_IMPORTED_MODULE_2__services_alertservice_service__["a" /* AlertService */]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/not-found/not-found.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotFoundComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/************ DEFINE PACKAGE AND STUFF ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// Stuff we need

/************ NOTFOUND COMPONENT ************/
var NotFoundComponent = /** @class */ (function () {
    function NotFoundComponent() {
    }
    NotFoundComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-not-found',
            template: "\n  <h1 class=\"display-1 text-center\">Oops!</h1>\n  <blockquote class=\"blockquote text-center\">\n    <p class=\"mb-0\">Sorry, this page doesn't exist</p>\n    <footer class=\"blockquote-footer\">Team HAM-Bot</footer>\n  </blockquote>\n  "
        })
    ], NotFoundComponent);
    return NotFoundComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/settings/settings.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "body {\r\n  padding-bottom: 65px;\r\n}\r\n\r\n.pointer {\r\n  cursor: pointer;\r\n}\r\n\r\n.fix-bottom {\r\n  margin-bottom: 20px;\r\n}\r\n\r\n.main-console{\r\n  background-color: #e6e6e6;\r\n  border-radius: .25rem;\r\n}\r\n\r\n.console-options {\r\n  margin: 0;\r\n  padding: 4px;\r\n  margin-bottom: 20px;\r\n  border-top: 2px solid rgba(0, 0, 0, 0.125);\r\n}\r\n\r\n.console {\r\n  background-color: #e6e6e6;\r\n  border-radius: 4px;\r\n  height: 250px;\r\n  width: 100%;\r\n  overflow: auto;\r\n  text-align: left;\r\n  padding-left: 15px;\r\n  padding: 10px;\r\n  margin-bottom: 10px;\r\n}\r\n\r\n.console p {\r\n  margin: 7px !important;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/settings/settings.component.html":
/***/ (function(module, exports) {

module.exports = "<h1 class=\"display-4 text-center\">Settings</h1>\n<hr>\n<div class=\"row\">\n  <div class=\"col-xl fix-bottom\">\n    <h4>Logfiles <span (click)=\"getAllLogFiles()\"><i class=\"fas fa-sync pointer\" aria-hidden=\"true\"></i></span></h4>\n    <p>Here is a list of all logfiles of recent prints, listed from new-old</p>\n    <div id=\"accordion\" role=\"tablist\" *ngFor=\"let logfile of logfiles | newtoold\" style=\"margin-bottom: .2em;\">\n      <div class=\"card\">\n        <div class=\"card-header\" role=\"tab\" id=\"headingOne\">\n          <h5 class=\"mb-0\">\n            <a data-toggle=\"collapse\" href=\"#{{ logfile.id }}\" aria-expanded=\"true\" attr.aria-controls=\"{{ logfile.id }}\" (click)=\"loadLogFile(logfile.filename)\">\n              {{ logfile.filename }}\n            </a>\n            <span (click)=\"deleteLogFile(logfile.filename)\"><i class=\"fas fa-trash float-right pointer\" aria-hidden=\"true\" ></i></span>\n          </h5>\n        </div>\n        <div id=\"{{ logfile.id }}\" class=\"collapse\" role=\"tabpanel\" aria-labelledby=\"headingOne\" data-parent=\"#accordion\">\n          <div class=\"card-body\">\n            <div class=\"load\" *ngIf=\"!logfiletext\">\n              <div class=\"bar\"></div>\n              <div class=\"bar\"></div>\n              <div class=\"bar\"></div>\n            </div>\n            <pre *ngIf=\"logfiletext\">{{ logfiletext }}</pre>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-xl fix-bottom\">\n    <!-- Comport -->\n    <h4>Change serial ports <span (click)=\"getSettings()\"><i class=\"fas fa-sync pointer\" aria-hidden=\"true\"></i></span></h4>\n    <hr>\n    <h5>GRBL Port</h5>\n    <div class=\"form-group row\" style=\"margin-top: 1em;\">\n      <label for=\"comportChange1\" class=\"col-lg col-form-label\">\n        <h6>GRBL connected on <strong>{{ connectedPortGRBL }}</strong></h6>\n      </label>\n      <div class=\"col\">\n        <div class=\"input-group mb-3\">\n          <select ngControl=\"type\" #GRBLserialportinput class=\"custom-select\" id=\"comportChange1\">\n            <option *ngFor=\"let comPort of comPorts\" [value]=\"comPort\" >{{ comPort }}</option>\n          </select>\n          <div class=\"input-group-append\">\n            <label class=\"input-group-text pointer\" for=\"comportChange1\" (click)=\"changeSerialPort('grbl', GRBLserialportinput.value)\">Change Port</label>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <h5>LCD Port</h5>\n    <div class=\"form-group row\" style=\"margin-top: 1em;\">\n      <label for=\"comportChange2\" class=\"col-lg col-form-label\">\n        <h6>LCD connected on <strong>{{ connectedPortLCD }}</strong></h6>\n      </label>\n      <div class=\"col\">\n        <div class=\"input-group mb-3\">\n          <select ngControl=\"type\" #LCDserialportinput class=\"custom-select\" id=\"comportChange2\">\n            <option *ngFor=\"let comPort of comPorts\" [value]=\"comPort\" >{{ comPort }}</option>\n          </select>\n          <div class=\"input-group-append\">\n            <label class=\"input-group-text pointer\" for=\"comportChange2\" (click)=\"changeSerialPort('lcd', LCDserialportinput.value)\">Change Port</label>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <hr>\n\n    <!-- Custom Commands to Raspberry Pi -->\n    <h4>Custom Commands for Raspberry Pi</h4>\n    <hr>\n    <div class=\"row\">\n      <div class=\"col-md-8\">\n        <div class=\"main-console\">\n          <pre class=\"console\">\n              <p *ngFor=\"let message of messages\"><span [ngStyle]=\"{'color': message.color}\">{{ message.message }}</span></p>\n          </pre>\n          <div class=\"console-options\">\n            <div class=\"input-group\">\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"commadInput\" #line (keyup.enter)=\"sendCommand(line.value)\" placeholder=\"Command\" aria-label=\"Command\">\n              <div class=\"input-group-append\">\n                <span class=\"input-group-text btn pointer\" (click)=\"sendCommand(line.value)\">Send</span>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-md\">\n        <button type=\"button\" class=\"btn btn-danger btn-block\" (click)=\"sendCommand('sudo shutdown now')\">Shutdown</button>\n          <hr>\n        <button type=\"button\" class=\"btn btn-success btn-block\" (click)=\"sendCommand('sudo reboot')\">Restart</button>\n          <hr>\n        <button type=\"button\" class=\"btn btn-info btn-block\" (click)=\"sendCommand('sudo service hambot restart')\">Restart Website</button>\n          <hr>\n        <button type=\"button\" class=\"btn btn-info btn-block\" (click)=\"sendCommand('sudo service motion restart')\">Restart Videostream</button>\n      </div>\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/settings/settings.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_alertservice_service__ = __webpack_require__("../../../../../src/app/services/alertservice.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_websocket_service__ = __webpack_require__("../../../../../src/app/services/websocket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_request_service__ = __webpack_require__("../../../../../src/app/services/api-request.service.ts");
/************ DEFINE PACKAGE AND STUFF ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Stuff we need

// Service Imports



/************ SETTINGS COMPONENT ************/
var SettingsComponent = /** @class */ (function () {
    function SettingsComponent(alertService, ws, API, el) {
        var _this = this;
        this.alertService = alertService;
        this.ws = ws;
        this.API = API;
        this.el = el;
        this.comPorts = [];
        this.logfiles = [];
        this.messages = [];
        // websocket initialization
        this.API.getWebSocketURL().subscribe(function (response) {
            _this.ws.WebsocketConnection(response.url).subscribe(function (data) {
                _this.handleMessage(data);
            });
        });
    }
    // call this funtions on initialization
    SettingsComponent.prototype.ngOnInit = function () {
        this.getAllLogFiles();
        this.getSettings();
    };
    // Function to get comports from server
    SettingsComponent.prototype.getSettings = function () {
        this.ws.send({ type: "list-comports" });
    };
    // Function to request port change
    SettingsComponent.prototype.changeSerialPort = function (name, port) {
        this.ws.send({ type: "change-comport", name: name, comPort: port });
    };
    // Function to send commands to the Linux console
    SettingsComponent.prototype.sendCommand = function (command) {
        this.ws.send({ type: "custom-command", command: command });
        this.commadInput = "";
    };
    // Function to handle messages from websocket
    SettingsComponent.prototype.handleMessage = function (data) {
        try {
            switch (data.type) {
                case 'comports': {
                    this.connectedPortGRBL = data.message.connectedPortGRBL;
                    this.connectedPortLCD = data.message.connectedPortLCD;
                    // Push all comPorts into array
                    this.comPorts = []; // make sure array is empty
                    for (var count = 0; count < data.message.ports.length; count++) {
                        this.comPorts.push(data.message.ports[count].comName);
                    }
                    break;
                }
                case 'console-response': {
                    if (this.messages.length >= 80)
                        this.messages.splice(0, 1);
                    if (data.message.message.length > 80) {
                        data.message.message = data.message.message.substring(0, 80) + "\n" + data.message.message.substring(80);
                    }
                    this.messages.push(data.message);
                    // Always scroll to bottom automatically
                    var scrollPane = this.el.nativeElement.querySelector('.console');
                    // check if console is active (error on mobile otherwise)
                    if (scrollPane)
                        scrollPane.scrollTop = scrollPane.scrollHeight;
                    break;
                }
                case 'success': {
                    this.alertService.newAlert('success', data.message);
                    break;
                }
                case 'info': {
                    this.alertService.newAlert('info', data.message);
                    break;
                }
                case 'error': {
                    this.alertService.newAlert('danger', data.message);
                    break;
                }
                case 'warning': {
                    this.alertService.newAlert('warning', data.message);
                    break;
                }
            }
        }
        catch (exception) {
            this.alertService.newAlert('danger', 'Angular handleMessage: Excetion: ' + exception);
        }
    };
    // get all logfiles
    SettingsComponent.prototype.getAllLogFiles = function () {
        var _this = this;
        this.API.getAllLogFiles().subscribe(function (response) {
            if (response.success) {
                _this.logfiles = response.message;
                // create id for element without . (/g means global so every . gets removed)
                for (var i = 0; i < _this.logfiles.length; i++) {
                    _this.logfiles[i].id = _this.logfiles[i].filename.replace(/[.]/g, '_');
                }
            }
        });
    };
    // load specific logfile (content)
    SettingsComponent.prototype.loadLogFile = function (filename) {
        var _this = this;
        this.logfiletext = ""; //remove old text
        // requests new text
        this.API.textFromLogFile(filename).subscribe(function (response) {
            _this.logfiletext = response;
        });
    };
    // delete logfile
    SettingsComponent.prototype.deleteLogFile = function (filename) {
        var _this = this;
        this.API.deleteLogFile(filename).subscribe(function (response) {
            if (response.success) {
                _this.getAllLogFiles();
                return _this.alertService.newAlert('success', response.message);
            }
            return _this.alertService.newAlert('danger', response.message);
        });
    };
    SettingsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-settings',
            template: __webpack_require__("../../../../../src/app/components/settings/settings.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/settings/settings.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_alertservice_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_2__services_websocket_service__["a" /* WebsocketService */],
            __WEBPACK_IMPORTED_MODULE_3__services_api_request_service__["a" /* ApiRequestService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */]])
    ], SettingsComponent);
    return SettingsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/userdb/userdb.component.html":
/***/ (function(module, exports) {

module.exports = "<h1 class=\"display-4 text-center\">UserDB</h1>\n<hr>\n<div class=\"row\">\n  <div class=\"col-lg\">\n    <h4 class=\"text-center\">Register a new User</h4>\n    <hr>\n      <div class=\"form-group row\">\n        <label for=\"Username\" class=\"col-md-2 col-form-label\">Username</label>\n        <div class=\"col-md-10\">\n          <input class=\"form-control\" type=\"text\" placeholder=\"Username\" [(ngModel)]=\"username\" (keyup.enter)=\"onRegisterSubmit()\" name=\"username\" required>\n        </div>\n      </div>\n      <div class=\"form-group row\">\n        <label for=\"Password\" class=\"col-md-2 col-form-label\">Password</label>\n        <div class=\"col-md-10\">\n          <input class=\"form-control\" type=\"password\" placeholder=\"Password\" [(ngModel)]=\"password\" (keyup.enter)=\"onRegisterSubmit()\" name=\"password\" required>\n        </div>\n      </div>\n      <button class=\"btn btn-primary btn-block\" (click)=\"onRegisterSubmit()\">Send</button>\n    <hr>\n    <h5 class=\"text-center\">A registered user has full access to the machine</h5>\n    <hr>\n  </div> <!-- col end -->\n  <div class=\"col-lg\">\n    <h4 class=\"text-center\">Registered Users</h4>\n    <hr>\n    <table class=\"table\">\n      <thead>\n        <tr>\n          <th>#</th>\n          <th>Username</th>\n          <th>Created on</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor=\"let user of users\">\n          <th scope=\"row\">{{ user.id }}</th>\n          <td>{{ user.username }}</td>\n          <td>{{ user.created | date: \"dd.MM.yyyy HH:mm:ss\" }}</td>\n          <td>\n            <div class=\"btn-group btn-group-sm\" role=\"group\" aria-label=\"User\">\n              <button type=\"button\" class=\"btn btn-secondary\" (click)=\"editUser(user.dbId, user.username)\">\n                <i class=\"fas fa-edit\" aria-hidden=\"true\"></i>\n              </button>\n              <button type=\"button\" class=\"btn btn-secondary\" (click)=\"deleteUser(user.dbId, user.username)\">\n                <i class=\"fas fa-times\" aria-hidden=\"true\"></i>\n              </button>\n            </div>\n          </td>\n        </tr>\n      </tbody>\n    </table>\n  </div><!-- col end -->\n</div><!-- row end -->\n"

/***/ }),

/***/ "../../../../../src/app/components/userdb/userdb.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserEditor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return UserdbComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_userauth_service__ = __webpack_require__("../../../../../src/app/services/userauth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_alertservice_service__ = __webpack_require__("../../../../../src/app/services/alertservice.service.ts");
/************ DEFINE PACKAGE AND STUFF ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Stuff we need


// Service Imports


/************ USER-EDITOR COMPONENT ************/
var UserEditor = /** @class */ (function () {
    function UserEditor(userAuthService, alertService, activeModal) {
        this.userAuthService = userAuthService;
        this.alertService = alertService;
        this.activeModal = activeModal;
    }
    // Function that get's called on submit
    UserEditor.prototype.editUserSubmit = function (id, password1, password2) {
        var _this = this;
        if (password1 == "" || password2 == "")
            return this.alertService.newAlert('danger', 'One of the inputs or both are empty!');
        if (password1 != password2)
            return this.alertService.newAlert('danger', 'Passwords do not match!');
        var user = {
            id: id,
            newPassword: password1
        };
        this.userAuthService.edit(user).subscribe(function (response) {
            if (!response.success) {
                _this.alertService.newAlert('danger', response.message);
            }
            else {
                _this.activeModal.close('Close click');
                _this.alertService.newAlert('success', response.message);
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], UserEditor.prototype, "username", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], UserEditor.prototype, "id", void 0);
    UserEditor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'user-editor',
            template: "\n    <div class=\"modal-header\">\n      <h4 class=\"modal-title\">Editing {{ username }}</h4>\n      <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n    </div>\n    <div class=\"modal-body\">\n      <form (submit)=\"onEditUserSubmit()\">\n        <div class=\"form-group row\">\n          <label for=\"User Database ID\" class=\"col-4 col-form-label\">\n            ID <span aria-hidden=\"true\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"ID cannot be changed\"><i class=\"fas fa-info-circle\"></i></span>\n          </label>\n          <div class=\"col-8\">\n            <input class=\"form-control\" type=\"text\" [value]=\"id\" disabled>\n          </div>\n        </div>\n        <div class=\"form-group row\">\n          <label for=\"Username\" class=\"col-4 col-form-label\">\n            Username <span aria-hidden=\"true\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Username cannot be changed\"><i class=\"fas fa-info-circle\"></i></span>\n          </label>\n          <div class=\"col-8\">\n            <input class=\"form-control\" type=\"text\" [value]=\"username\" disabled>\n          </div>\n        </div>\n        <div class=\"form-group row\">\n          <label for=\"New Password\" class=\"col-4 col-form-label\">New Password</label>\n          <div class=\"col-8\">\n            <input class=\"form-control\" type=\"password\" placeholder=\"New Password\" #password1 (keyup.enter)=\"editUserSubmit(id, password1.value, password2.value)\">\n          </div>\n        </div>\n        <div class=\"form-group row\">\n          <label for=\"Confirm New Password\" class=\"col-4 col-form-label\">Confirm Password</label>\n          <div class=\"col-8\">\n            <input class=\"form-control\" type=\"password\" placeholder=\"Confirm Password\"  #password2 (keyup.enter)=\"editUserSubmit(id, password1.value, password2.value)\">\n          </div>\n        </div>\n      </form>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"submit\" class=\"btn btn-primary\" (click)=\"editUserSubmit(id, password1.value, password2.value)\">Send</button>\n      <button type=\"button\" class=\"btn btn-secondary\" (click)=\"activeModal.close('Close click')\">Cancel</button>\n    </div>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_userauth_service__["a" /* UserauthService */],
            __WEBPACK_IMPORTED_MODULE_3__services_alertservice_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* NgbActiveModal */]])
    ], UserEditor);
    return UserEditor;
}());

/************ USERDB COMPONENT ************/
var UserdbComponent = /** @class */ (function () {
    function UserdbComponent(userAuthService, alertService, modalService) {
        this.userAuthService = userAuthService;
        this.alertService = alertService;
        this.modalService = modalService;
    }
    UserdbComponent.prototype.ngOnInit = function () { this.getAllUsers(); };
    // Register Handler
    UserdbComponent.prototype.onRegisterSubmit = function () {
        var _this = this;
        var user = {
            username: this.username,
            password: this.password
        };
        this.userAuthService.register(user).subscribe(function (response) {
            if (!response.success)
                _this.alertService.newAlert('danger', response.message);
            else {
                _this.getAllUsers();
                _this.alertService.newAlert('success', response.message);
                _this.username = "";
                _this.password = "";
                return;
            }
        });
    };
    // Function to request a user edit
    UserdbComponent.prototype.editUser = function (id, username) {
        var modalRef = this.modalService.open(UserEditor);
        modalRef.componentInstance.id = id;
        modalRef.componentInstance.username = username;
    };
    // Function to request a user delete
    UserdbComponent.prototype.deleteUser = function (id, username) {
        var _this = this;
        var user = {
            id: id,
            username: username
        };
        this.userAuthService.delete(user).subscribe(function (response) {
            if (!response.success) {
                _this.alertService.newAlert('danger', response.message);
            }
            else {
                _this.getAllUsers();
                _this.alertService.newAlert('success', response.message);
            }
        });
    };
    // Handler to get all users from server
    UserdbComponent.prototype.getAllUsers = function () {
        var _this = this;
        this.userAuthService.getAllUsers().subscribe(function (response) {
            if (!response.success)
                _this.alertService.newAlert('danger', response.message);
            else
                _this.users = response.users;
        });
    };
    UserdbComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-userdb',
            template: __webpack_require__("../../../../../src/app/components/userdb/userdb.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_userauth_service__["a" /* UserauthService */],
            __WEBPACK_IMPORTED_MODULE_3__services_alertservice_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */]])
    ], UserdbComponent);
    return UserdbComponent;
}());



/***/ }),

/***/ "../../../../../src/app/pipes/format-time.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormatTimePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/************ DEFINE PACKAGE AND STUFF ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// Stuff we need

var FormatTimePipe = /** @class */ (function () {
    function FormatTimePipe() {
    }
    FormatTimePipe.prototype.transform = function (value, args) {
        var hours = Math.floor(value / 3600);
        value -= hours * 3600;
        var minutes = Math.floor(value / 60);
        value -= minutes * 60;
        var seconds = value % 60;
        return (hours + ':' + (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds < 10 ? '0' + seconds : seconds));
    };
    FormatTimePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Pipe */])({
            name: 'formatTime'
        })
    ], FormatTimePipe);
    return FormatTimePipe;
}());



/***/ }),

/***/ "../../../../../src/app/pipes/newtoold.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewtooldPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
// https://stackoverflow.com/questions/41125311/sort-by-date-angular-2-pipe
/************ DEFINE PACKAGE AND STUFF ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// Stuff we need

var NewtooldPipe = /** @class */ (function () {
    function NewtooldPipe() {
    }
    NewtooldPipe.prototype.transform = function (file) {
        // fix error on init
        if (!file || file.length < 1)
            return;
        // sort
        var NewToOld = file.sort(function (a, b) {
            var date1 = new Date(a.created);
            var date2 = new Date(b.created);
            if (date1 < date2) {
                return 1;
            }
            else if (date1 > date2) {
                return -1;
            }
            else {
                return 0;
            }
        });
        return NewToOld;
    };
    NewtooldPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Pipe */])({
            name: 'newtoold'
        })
    ], NewtooldPipe);
    return NewtooldPipe;
}());



/***/ }),

/***/ "../../../../../src/app/pipes/searchfilter.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchFilterPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/************ DEFINE PACKAGE AND STUFF ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// Stuff we need

var SearchFilterPipe = /** @class */ (function () {
    function SearchFilterPipe() {
    }
    SearchFilterPipe.prototype.transform = function (files, criteria) {
        // sort names from A-Z
        files = files.sort(this.sortOn("filename"));
        if (!criteria || criteria == "" || criteria == undefined || criteria == null)
            return files;
        // returns files with filterd by criteria
        return files.filter(function (file) {
            for (var temp in file) {
                if (file.filename == null)
                    continue;
                if (file.filename.toLowerCase().includes(criteria.toLowerCase()))
                    return true;
            }
            return false;
        });
    };
    // Function to sort filenames from A-Z
    SearchFilterPipe.prototype.sortOn = function (property) {
        return function (a, b) {
            if (a[property].toLowerCase() < b[property].toLowerCase())
                return -1;
            else if (a[property].toLowerCase() > b[property].toLowerCase())
                return 1;
            else
                return 0;
        };
    };
    SearchFilterPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Pipe */])({
            name: 'searchFilter'
        })
    ], SearchFilterPipe);
    return SearchFilterPipe;
}());



/***/ }),

/***/ "../../../../../src/app/services/alertservice.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("../../../../rxjs/_esm5/Subject.js");
/************ DEFINE PACKAGE AND STUFF ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// Stuff we need


var AlertService = /** @class */ (function () {
    function AlertService() {
        this.id = 0;
        this.alerts = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["a" /* Subject */]();
    }
    // Function to get all Alerts from observable
    AlertService.prototype.getAlerts = function () {
        return this.alerts.asObservable();
    };
    // Function to create new Alert
    AlertService.prototype.newAlert = function (type, message) {
        this.alerts.next({ 'id': this.id++, 'type': type, 'message': message });
    };
    AlertService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])()
    ], AlertService);
    return AlertService;
}());



/***/ }),

/***/ "../../../../../src/app/services/api-request.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiRequestService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/************ DEFINE PACKAGE AND STUFF ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Stuff we need



var ApiRequestService = /** @class */ (function () {
    function ApiRequestService(http) {
        this.http = http;
    }
    // Function to get all files from server
    ApiRequestService.prototype.getAllFiles = function () {
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].APIUrl + "/files/";
        return this.http.get(url, { responseType: 'json' });
    };
    // Function to download file from server
    ApiRequestService.prototype.downloadFile = function (filename) {
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].APIUrl + "/file/download/" + filename;
        return this.http.get(url, { responseType: 'blob' });
    };
    // Funtion to get text from file
    ApiRequestService.prototype.textFromFile = function (filename) {
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].APIUrl + "/file/download/" + filename;
        return this.http.get(url, { responseType: 'text' });
    };
    // Function to upload file to server
    ApiRequestService.prototype.uploadFile = function (file) {
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].APIUrl + "/file/upload/";
        return this.http.post(url, file, { responseType: 'json' });
    };
    // Function to edit file
    ApiRequestService.prototype.editFile = function (file) {
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].APIUrl + "/file/edit/" + file.filename;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' });
        return this.http.post(url, file, { headers: headers, responseType: 'json' });
    };
    // Function to delete file
    ApiRequestService.prototype.deleteFile = function (filename) {
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].APIUrl + "/file/delete/" + filename;
        return this.http.delete(url, { responseType: 'json' });
    };
    // Function to request the websocket URL we connect to
    ApiRequestService.prototype.getWebSocketURL = function () {
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].APIUrl + "/websocket";
        return this.http.get(url, { responseType: 'json' });
    };
    // Function to get all logfiles from server
    ApiRequestService.prototype.getAllLogFiles = function () {
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].APIUrl + "/logfiles/";
        return this.http.get(url, { responseType: 'json' });
    };
    // Function to get logfile from server
    ApiRequestService.prototype.textFromLogFile = function (filename) {
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].APIUrl + "/logfiles/" + filename;
        return this.http.get(url, { responseType: 'text' });
    };
    // Function to delete logfile
    ApiRequestService.prototype.deleteLogFile = function (filename) {
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].APIUrl + "/logfiles/" + filename;
        return this.http.delete(url, { responseType: 'json' });
    };
    ApiRequestService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], ApiRequestService);
    return ApiRequestService;
}());



/***/ }),

/***/ "../../../../../src/app/services/auth-guard.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuardService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__userauth_service__ = __webpack_require__("../../../../../src/app/services/userauth.service.ts");
/************ DEFINE PACKAGE AND STUFF ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Stuff we need


// Services

var AuthGuardService = /** @class */ (function () {
    function AuthGuardService(userAuthService, router) {
        this.userAuthService = userAuthService;
        this.router = router;
    }
    AuthGuardService.prototype.canActivate = function () {
        var _this = this;
        return this.userAuthService.loginState().map(function (response) {
            if (!response.success) {
                _this.router.navigate(['/']);
                return false;
            }
            return true;
        });
    };
    AuthGuardService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__userauth_service__["a" /* UserauthService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]])
    ], AuthGuardService);
    return AuthGuardService;
}());



/***/ }),

/***/ "../../../../../src/app/services/userauth.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserauthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/************ DEFINE PACKAGE AND STUFF ************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Stuff we need



var UserauthService = /** @class */ (function () {
    function UserauthService(http) {
        this.http = http;
    }
    // Function to get all users from server
    UserauthService.prototype.getAllUsers = function () {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].userAPIUrl);
    };
    // Function to register new user
    UserauthService.prototype.register = function (user) {
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].userAPIUrl + "/register";
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' });
        return this.http.post(url, user, { headers: headers });
    };
    // Function to login
    UserauthService.prototype.login = function (user) {
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].userAPIUrl + "/authenticate";
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' });
        return this.http.post(url, user, { headers: headers });
    };
    // Function to edit user
    UserauthService.prototype.edit = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' });
        return this.http.put(__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].userAPIUrl, user, { headers: headers });
    };
    // Function to delete user
    UserauthService.prototype.delete = function (user) {
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' });
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].userAPIUrl + "/" + user.id + "/" + user.username;
        return this.http.delete(url, { headers: headers });
    };
    // Function to logout
    UserauthService.prototype.logout = function () {
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].userAPIUrl + "/logout";
        return this.http.get(url);
    };
    // Function to recieve loginstate from server
    UserauthService.prototype.loginState = function () {
        var url = __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].userAPIUrl + "/loginstate";
        return this.http.get(url);
    };
    UserauthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], UserauthService);
    return UserauthService;
}());



/***/ }),

/***/ "../../../../../src/app/services/websocket.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WebsocketService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_share__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/share.js");
// http://plnkr.co/edit/MjZA5HOfllvMPjoCxe3I?p=preview&open=app%2Fapp.component.ts
// https://stackoverflow.com/questions/36851347/angular2-websocket-how-to-return-an-observable-for-incoming-websocket-messages
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/************ DEFINE PACKAGE AND STUFF ************/
// Stuff we need




var WebsocketService = /** @class */ (function () {
    function WebsocketService() {
    }
    // Function to send data to websocket server
    WebsocketService.prototype.send = function (text) {
        var _this = this;
        if (this.websocket && this.websocket.readyState)
            this.websocket.send(JSON.stringify(text));
        else
            setTimeout(function () {
                _this.send(text);
            }, 100);
    };
    // Function to connect to websocket server
    WebsocketService.prototype.WebsocketConnection = function (websocketURL) {
        var _this = this;
        // Checks if already connected to websocket
        if (!this.websocket || !this.websocket.readyState) {
            console.log("Trying to connect to server");
            this.websocket = new WebSocket(websocketURL);
            // Websocket Events
            this.websocket.onopen = function (evt) {
                console.log("Connection established");
            };
        }
        // Close handler
        this.websocket.onclose = function (evt) {
            alert("Websocket connection closed, please reload the page to reconnect!");
        };
        // Error handler
        this.websocket.onerror = function (evt) {
            alert("Error: " + evt);
        };
        // return messages from server to function
        return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["a" /* Observable */].create(function (observer) {
            _this.websocket.onmessage = function (evt) {
                observer.next(evt);
            };
        })
            .map(function (res) { return JSON.parse(res.data); })
            .share();
    };
    WebsocketService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])()
    ], WebsocketService);
    return WebsocketService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true,
    APIUrl: window.location.origin + '/api',
    userAPIUrl: window.location.origin + "/api/user"
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_16" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map