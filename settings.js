var settings = module.exports = {};

settings.webserverPort = 80;
settings.webserverIP = "http://127.0.0.1:4200";
settings.websocketIP = "ws://127.0.0.1:80/";

settings.GRBL_serialBaudRate = 115200;
settings.GRBL_serialport = "COM3";

settings.LCD_serialBaudRate = 115200;
settings.LCD_serialport = "COM7"

settings.filesdir = './files/';
