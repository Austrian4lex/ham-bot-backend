/************ DEFINE PACKAGE AND STUFF ************/

// define packages
const fs = require('fs');
const parse = require('csv-parse');

// define path of CSV files
const alarm_codes =   "./controllers/csv/alarm_codes_en_US.csv";
const error_codes =   "./controllers/csv/error_codes_en_US.csv";
const setting_codes = "./controllers/csv/setting_codes_en_US.csv";




/************ FUNCTIONS FOR SERIALDATA ************/


// variables
var settings_description = [];
var settingsStatus = false;


// Save Setting Codes on Server start (otherwise Settingsdescription would be wrong)
loadSettingsDescription();


// Main function to handle serial data
exports.handleData = function (data, callback) {
  // to get rid of empty lines
  data = data.trim();

  // Only use first char of string
  char = data.substring(0, 1);

  switch (char) {
    // ok handler
    case 'o':
      return callback(null, { type: "myConsole", data: { message: data, color: "green" }, status: "ok" });

    // error handler
    case 'e':
      handleError(data, (errormessage) => {
        if (!errormessage)
          return callback("Unknown Error: Errormessage");
        else
          return callback(null, { type: "myConsole", data: { message: errormessage, color: "red" }, status: "error" });
      });
      break;

    // Handle Machine Status from '?'
    case '<':
      // remove <>
      data = data.substring(1 , data.length - 1);
      // Idle|MPos:0.000,0.000,0.000|FS:0,0
      // split data
      var splitted_data = data.split('|');
      var status = splitted_data[0];
      var coor = splitted_data[1].split(':')[1].split(',');
      var feed_rate = splitted_data[2].split(":")[1].split(",")[0]; // split for feedrate (S would be spindle speed)
      return callback(null, { type: "machineStatus", data: { status_machine: status, coorX: coor[0], coorY: coor[1], feed_rate: feed_rate} });

    // Handle Grbl X.Xx [....]
    case 'G':
      return callback(null, { type: "myConsole", data: { message: data, color: "black"} });

    // Alarm Handler
    case 'A':
      handleAlarm(data, (alarmmessage) => {
        if (!alarmmessage)
          return callback('Unknown Error: Alarmmessage');
        else
          return callback(null, { type: "myConsole", data: { message: alarmmessage, color: "orange" } });
      });
      break;
    // Handle Setting Printout
    case '$':
      handleSettingsData(data, (setting) => {
        if (!setting)
          return callback('Unknown Error: Settingsdescription');
        else
          return callback(null, { type: "myConsole", data: { message: setting, color: "black" } });
      });
      break;

    case '[':
      handleNonReqFeedback(data, (data) => {
        return callback(null, { type: "myConsole", data: { message: data, color: "black" } });
      });
      break;
  }
}


// Function to add description to error code
function handleError(error, callback) {

  var code = error.split(':')[1];
  var errors = [];
  var line;

  fs.createReadStream(error_codes)
      .pipe(parse({delimiter: ','}))
      .on('data', (csvrow) => {
          errors.push(csvrow);
      })
      .on('end', () => {
        // Loop through all error descriptions
        for (var i = 0; i < errors.length; i++) {
          if (!errors[i].indexOf(code)) {
            line = i;
            break;
          }
        }
        return callback(error + " (" + errors[line][2] + ")");
      });
}


// Function to add description to alarm code
function handleAlarm(alarm, callback) {

  var code = alarm.split(':')[1];
  var alarms = [];
  var line;

  fs.createReadStream(alarm_codes)
      .pipe(parse({delimiter: ','}))
      .on('data', (csvrow) => {
          alarms.push(csvrow);
      })
      .on('end', () => {
        // Loop through all alarm descriptions
        for (var i = 0; i < alarms.length; i++) {
          if (!alarms[i].indexOf(code)) {
            line = i;
            break;
          }
        }
        return callback(alarm + " (" + alarms[line][2] + ")");
      });
}


// Function to add Settingsdescription to setting
function handleSettingsData(data, callback) {

  // Check for $N (startup blocks)
  var startup = data.split('=')[0].substring(0);
  if (startup == "$N0" || startup == "$N1")
    return callback(data);

  // Checks if settings were loaded
  if (!settingsStatus)
    loadSettingsDescription();

  var code = data.split('=')[0].substring(1);
  var line;

  // Loop through all settings descriptions
  for (var i = 0; i < settings_description.length; i++) {
    if (!settings_description[i].indexOf(code)) {
      line = i;
      break;
    }
  }
  return callback(data + " (" + settings_description[line][3] + ")");
}


// Function to add description to
function handleNonReqFeedback(data, callback) {
  // csv buggy -> non-reqfeedback not important
  return callback(data);
}


// Load Settings Description from CSV
function loadSettingsDescription() {
  fs.createReadStream(setting_codes)
      .pipe(parse({delimiter: ','}))
      .on('data', (csvrow) => {
          settings_description.push(csvrow);
      })
      .on('end', () => {
        settingsStatus = true;
      });
}
