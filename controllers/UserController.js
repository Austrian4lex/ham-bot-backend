/************ DEFINE PACKAGE AND STUFF ************/

// define packages
const crypto = require('crypto');
const moment = require('moment');
const mongoose = require('mongoose');
const userModel = require('../models/user');

// connect to mongodb via mongoose
mongoose.connect('mongodb://127.0.0.1/ham_bot');




/************ FUNCTIONS FOR HANDLING USER REQUESTS ************/


// Function to verif token + to get username of token
exports.verifyToken = function (token, callback) {

  // Checks if token was provided
  if (!token)
    return callback({ success: false, message: "No Token provided!" });

  var query = userModel.where({ token: token });
  // uses the defined query from above to find user in DB
  query.findOne((error, user) => {
    if (error)
      return callback({ success: false, message: 'DB Error!: ' + error });

    // checks if user was found
    if (user)
      return callback({ success: true, message: user.username});
    else
      return callback({ success: false, message: "Invalid Token!" });
  });

}


// Login Function
exports.login = function (username, password, callback) {

  // Checks if username and password are provided
  if (username == "" || password == "" || username == null || password == null)
    return callback({ success: false, message: "No Username/Password was provided!" });

  username = usernameSchema(username);

  var query = userModel.where({ username: username });
  // uses the defined query from above to find user in DB
  query.findOne((error, user) => {
    if (error)
      return callback({ success: false, message: 'DB Error!: ' + error });

    // checks if user exists (error if user is not found)
    if (user) {
      // hashes the sent password with the Salt
      crypto.pbkdf2(password, user.salt, 1000, 512, 'sha512', (error, hash) => {
          if (error)
            return callback({ success: false, message: "Hashing Error: " + error });
          // checks if passwords are matching
          if (hash.toString("hex") == user.password)
            return callback({ success: true, message: "Login succeeded", username: user.username, token: user.token });
          else
            return callback({ success: false, message: "Password is wrong!" });
      });
    } else
        return callback({ success: false, message: "Username was not found!" });
  });

}

exports.register = function (username, password, callback) {

  // Checks if username and password are provided
  if (username == "" || password == "" || username == null || password == null)
    return callback({ success: false, message: "No Username/Password was provided!" });

  var query = userModel.where({username: username});
  // uses the defined query from above to find user in DB
  query.findOne((error, user) => {
    if (error)
      return callback({ success: false, message: 'DB Error!: ' + error });

    // checks if user already exists (error if user is found --> true)
    if (user) {
      return callback({ success: false, message: "Username already exists" });
    } else {
      // creates a Salt for the passwordhasher
      var salt = createSalt();
      // Hashes the password with created Salt
      crypto.pbkdf2(password.toString(), salt, 1000, 512, 'sha512', (error, hash) => {
          if (error)
            return callback({success: false, message: "Hashing error: " + error});
          // creates token -> username:salt
          var token = new Buffer(username + ":" + createSalt()).toString('base64');
          // create UserModel for DB
          var newUser = new userModel({
            username: username,
            password: hash.toString("hex"),
            salt: salt,
            created: new Date,
            token: token
          });

          // Save new User to DB
          newUser.save((error) => {
            if (error)
              return callback({ success: false, message: "Saving to DB error: " + error });
            else
              return callback({ success: true, message: usernameSchema(username) + ' registered successfully' });
          });
      }); // crypto.pbkdf2 end
    }
  }); // query.find end
}


// function to get all registered users
exports.getAllUsers = function (callback) {
  userModel.find({}, (error, users) => {
    if (error)
      return callback({ success: false, message: error});
    //define array
    var usernames = [];

    // creats a new Array with only these items
    for (var i = 0; i < users.length; i++) {
      usernames.push({
        "id":        i,
        "username":  users[i].username,
        "created":   users[i].created,
        "dbId":      users[i].id
      });
    }
    return callback({ success: true, "users": usernames });
  });

}


// Update password for Username provided with ID
exports.updateUser = function (id, newPassword, callback) {

  // Checks if ID and newPassword are provided
  if (id == "" || newPassword == "" || id == null || newPassword == null)
    return callback({ success: false, message: "ID/Password where not provided" });

  var query = userModel.where({_id: id});
  // uses the define query from above to find user in DB
  query.findOne((error, user) => {
    // quick errorhandling
    if (error)
      return callback({ success: false, message: 'DB error!' + error });

    // checks if user exists (error if user is not found)
    if (user) {
      // hashes the sent password with the Salt
      crypto.pbkdf2(newPassword, user.salt, 1000, 512, 'sha512', (error, hash) => {
          if (error)
            return callback({ success: false, message: "Hashing error: " + error });
          // checks if new password matches old password
          if (hash.toString("hex") == user.password)
            return callback({ success: false, message: user.username + ", please use a new password (New Password matched old Password!)" });
          else {
            // creates new salt for new password
            var salt = createSalt();
            crypto.pbkdf2(newPassword, salt, 1000, 512, 'sha512', (error, hash) => {
              if (error)
                return callback({ success: false, message: "Hashing error: " + error });
              // saves new password and salt to the user
              userModel.findOneAndUpdate({_id: id}, { $set: { password: hash.toString("hex"), salt: salt } }, { new: true }, (error) => {
                  if (error)
                    return callback({ success: true, message: "findOneAndUpdate error: " + error });
                  else
                    return callback({ success: true, message: "Password was successfully updated for " + user.username });
              });
            });
          }
      });
    } else
        return callback({ success: true, message: "User was not found!" });
  });
}



// function to delete User specified by ID
exports.deleteUser = function (id, username, callback) {

  // Checks if ID and username are provided
  if (id == "" || username == "" || id == null || username == null)
    return callback({ success: false, message: "ID/Username where not provided" });

  // Remove user defined by id
  userModel.remove({_id: id}, (error) => {
    if (error)
      return callback({ success: false, message: error });
    else
      return callback({ success: true, message: username + " was successfully deleted" });
  });
}




/************ HELPER FUNCTIONS ************/

// Generating a Salt for the Password
function createSalt() {

  var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
  var salt = '';
	for (var i = 0; i < 25; i++) {
		var p = Math.floor(Math.random() * set.length);
		salt += set[p];
	}

	return salt;
}

// Function to format username
function usernameSchema(username) {
  return username.charAt(0).toUpperCase() + username.substring(1).toLowerCase();
}
