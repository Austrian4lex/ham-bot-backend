/************ DEFINE PACKAGE AND STUFF ************/

// define packages
const fs = require('fs');




/************ GCODE-ANALYZER ************/


// variables
var approxTime = 0;


// https://github.com/repetier/Repetier-Host/blob/master/src/RepetierHost/model/GCodeAnalyzer.cs
exports.GRBLAnalyzer = function (filename, callback) {

		callback({ success: true, message: filename + " is beeing analyzed!" });

		// Get GCODE from file
		fs.readFile('./files/' + filename, (error, data) => {
			if (error)
				return callback({ succes: false, message: "readFile Error | " + error });

			// Buffer --> String + split at new line
			data = data.toString().split("\n");

			var lines = [];
			var linesTotal = data;

			// variables
			var oldX = 0.00, oldY = 0.00;
			var newX = 0.00, newY = 0.00;
			var newI = 0.00, newJ = 0.00, newR = 0.00;
			var feedRate = 2000; // max default feedrate -> no divided by zero error

			// loop through every found line
			for (var i = 0; i < linesTotal.length; i++) {

				// Removes blank lines
				if (linesTotal[i] == "" || linesTotal[i] == " " ||linesTotal[i] == "\n" || linesTotal[i] == "\r" || linesTotal[i] == "\t" || linesTotal[i].length < 3)
					continue;
				else {
					// Removes commands + adds \n to end of line
					templine = linesTotal[i].replace(/\(.*?\)/g, "") + "\n";

					// if there is \r\n don't add line to queue
					if (templine == "\r\n")
						continue;
					else {
						// Remove any whitespaces
						templine = templine.replace(/\s/g, "");

						// Push the line in the array
						lines.push(templine + "\n");
					}

				} // else end

				var lineTotal = linesTotal[i];
				var commands = lineTotal.split(" ");

				var command = commands[0];

				// Check for LIN or CIRC to calculate approx time
				switch(command) {
					case "G0":
					case "G00":
					case "G1":
					case "G01":
						for (var l = 0; l < commands.length; l++) {
							if (commands[l] == "" || commands[l] == " " || commands[l] == "\n" || commands[l] == "\r" ) continue;
							if (commands[l].charAt(0) == "X")
								newX = parseFloat(commands[l].substring(1));

							if (commands[l].charAt(0) == "Y")
								newY =parseFloat(commands[l].substring(1));

							if (commands[l].charAt(0) == "F")
								feedRate = parseFloat(commands[l].substring(1));
						}

						var dx = Math.abs(newX - oldX);
						var dy = Math.abs(newY - oldY);

						approxTime += (Math.sqrt(dx * dx + dy * dy) * 60) / feedRate;

						oldX = newX;
						oldY = newY;
						break;
					case "G2":
					case "G02":
					case "G3":
					case "G03":
						var isclockwise = true;
						for (var l = 0; l < commands.length; l++) {
							if (commands[l] == "" || commands[l] == " " || commands[l] == "\n" || commands[l] == "\r" ) continue;
							if (commands[l].charAt(0) == "X") {
								newX = parseFloat(commands[l].substring(1));
							}
							if (commands[l].charAt(0) == "Y") {
								newY =parseFloat(commands[l].substring(1));
							}
							if (commands[l].charAt(0) == "I") {
								newI =parseFloat(commands[l].substring(1));
							}
							if (commands[l].charAt(0) == "J") {
								newJ =parseFloat(commands[l].substring(1));
							}
							if (commands[l].charAt(0) == "R") {
								newR = parseFloat(commands[l].substring(1));
							}
							if (commands[l].charAt(0) == "F") {
								feedRate = parseFloat(commands[l].substring(1));
							}
						}
						if (newR > 0) {

							var cx = newX - oldX;
							var cy = newY - oldY;

							var h_x2_div_d = -parseFloat(Math.sqrt(4 * newR * newR - cx * cx - cy * cy) / Math.sqrt(cx * cx + cy * cy)); // == -(h * 2 / d)

							if (command == 'G3' || command == "G03") {
								h_x2_div_d = -h_x2_div_d;
								isclockwise = false;
							}

							if (newR < 0)
							{
								h_x2_div_d = -h_x2_div_d;
								newR = -newR; // Finished with r. Set to positive for mc_arc
							}
							// Complete the operation by calculating the actual center of the arc
							newI = 0.5 * (cx - (cy * h_x2_div_d));
							newJ = 0.5 * (cy + (cx * h_x2_div_d));
						} else {
							newR = parseFloat(Math.sqrt((newI * newI) + (newJ * newJ)));
						}

						var center_axis0 = oldX + newI;
						var center_axis1 = oldY + newJ;

						var r_axis0 = -newI;  // Radius vector from center to current location
						var r_axis1 = -newJ;
						var rt_axis0 = newX - center_axis0;
						var rt_axis1 = newY - center_axis1;

						// CCW angle between position and target from circle center. Only one atan2() trig computation required.
						var angular_travel = Math.atan2(r_axis0 * rt_axis1 - r_axis1 * rt_axis0, r_axis0 * rt_axis0 + r_axis1 * rt_axis1);
						if (angular_travel < 0) { angular_travel += 2 * Math.PI; }
						if (isclockwise) { angular_travel -= 2 * Math.PI; }

						var millimeters_of_travel = Math.abs(angular_travel) * newR; //hypot(angular_travel*radius, fabs(linear_travel));
						if (millimeters_of_travel < 0.001) { return; }

						var time = (millimeters_of_travel / feedRate) * 60;
						approxTime += time;

						oldX = newX;
						oldY = newY;
						newR = 0;
						break;
					case 'G4':
					case 'G04':
						for (var l = 0; l < commands.length; l++) {
							if (commands[l] == "" || commands[l] == " " || commands[l] == "\n" || commands[l] == "\r" ) continue;
							if (commands[l].charAt(0) == "P") {
								approxTime += parseFloat(commands[l].substring(1));
							}
						}
						break;
				}

			}

			callback({
				success: true,
				message: filename + " analyzed! " + linesTotal.length + " line were found to stream",
				gcode: lines,
				filename: filename,
				approxTime: approxTime
			 });

			 return approxTime = 0;
		});

	}



// Global Helper Functions

function log(message) {
	message = '[' + new Date().toLocaleString() + '] ' + message;	// Add Date+Time to message
	// Add message to File -> if success write it to console
	fs.appendFile('./logFile.log', message + '\n', (error) => {
		if (error) throw error;
		else console.log(message);
	});
}
