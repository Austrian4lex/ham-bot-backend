/************ DEFINE PACKAGE AND OTHER STUFF ************/

// define packages
const mongoose = require('mongoose');
const Schema = mongoose.Schema;




/************ CREATE SCHEMA ************/

// User-Schema
const User = new Schema ({
  username: {type: String, trim: true, set: capitalize},
  password: String,
  salt: String,
  created: Date,
  token: String
});

function capitalize(val) {
  if ('string' != typeof val) val = '';
  return val.charAt(0).toUpperCase() + val.substring(1).toLowerCase();
}

// Export Usermodel
module.exports = mongoose.model('User', User);
