/************ DEFINE PACKAGE AND STUFF ************/

// defines config file
const settings = require('../settings');

 // define packages
const express = require('express');
const fs = require('fs');
const formidable = require('formidable');
const path = require('path');
const router = express.Router();

// define UserLogin model
const UserController = require('../controllers/UserController');




/************ FILE HANDLER ************/


// variables
var filesObject = []; // Array for GCODE filenames + info

// Routes to get Filenames
router.get('/files/', (req, res) => {

  if (!filesObject.message)
    getFilesAndStats();

  res.send({ success: true, message: filesObject });
  filesObject = [];
  return;
});


// Function to get all file + date stat
function getFilesAndStats() {
  var files = fs.readdir(settings.filesdir, (error, files) => {
    // loops through each file
    files.forEach((file) => {
      // Gets stat from specific file
      fs.stat(settings.filesdir + file, (error, statsObject) => {
        filesObject.push(({ filename: file, created: statsObject.ctime }));
      });
    });
  });
}

// Call function immediately to fix empty array bug
getFilesAndStats();


// File Download handler
router.get('/file/download/:file', (req, res) => {
  // Don't need to check for req.params.file -> no name provided, another handler handles it
  var file = __dirname + '/../files/' + req.params.file;
  // Checks if file exists
  if (fs.existsSync(file))
    res.download(file);
  else
    return res.send({ success: false, message: req.params.file + " not found!" });
});


// File Uploader Handler
router.post('/file/upload/', (req, res) => {
  Authorized(req, (response) => {
    if (!response.success) {
      return res.send({ success: false, message: "Please sign in to upload a file!" });
    } else {
      var filenames = [];
      var form = new formidable.IncomingForm();

      // Formidable config
      form.uploadDir = __dirname + "/../files/";
      form.keepExtension = true;
      form.multiples = true;

      // When file hits server
      form.on('file', (fields, file) => {
        filenames.push(file.name);
        console.log(file.name);
        try {
          fs.rename(file.path, path.join(form.uploadDir, file.name), (error) => {
            if (error) log(error);
          });
        } catch (error) {
          console.log(error);
        }

      });

      // Progress Event (optional because files are usually small)
      /*form.on('progress', (bytesReceived, bytesExpected) => {
        console.log("Progress: " + (bytesReceived / bytesExpected) * 100 + "%");
      });*/

      // End Event
      form.on('end', () => {
        var names = "";
        if (filenames.length > 1) {
          for (var i = 0; i < filenames.length; i++) {
            names += filenames[i] + " ";
          }
          res.send({ success: true, message: names + " were successfully uploaded", filename: filenames });
        }
        else
          res.send({ success: true, message: filenames[0] + " was successfully uploaded", filename: filenames });
        filenames = [];
        return;
      });

      // Error Event
      form.on('error', (error) => {
        log("Upload Error: "+ error);
        return res.send({ success: false, message: "Upload Error: "+ error })
      });

      // parse req into form handlers
      form.parse(req);
    }
  });

});


// File editor handler
router.post('/file/edit/:filename', (req, res) => {
  // Checks if Authorized
  Authorized(req, (response) => {
    if (response.success) {
      fs.writeFile("./files/" + req.params.filename, req.body.gcode.toString(), (error) => {
        if (error) {
          res.send({ success: false, message: error });
          log(error);
        } else
          res.send({success: true, message: req.params.filename + " changed successfully" });
      });
    } else
      res.send(response);
  });
});


// File Delete handler
router.delete('/file/delete/:file', (req, res) => {
  // Checks if Authorized
  Authorized(req, (response) => {
    if (response.success) {
      // Don't need to check for req.params.file -> no name provided, another handler handles it
      var file = __dirname + '/../files/' + req.params.file;
      // Checks if file exists
      if (fs.existsSync(file)) {
        // Deletes the File
        fs.unlink(file , (error) => {
          if (error) {
            log("Deleting File Error: " + error);
            return res.send({ success: false, message: "Error: " + error});
          } else {
            log(req.params.file + " was successfully deleted");
            return res.send({ success: true, message: req.params.file + " was successfully deleted" });
          }
        });
      } else
        return res.send({ success: false, message: req.params.file + " not found!" });
    } else
      return res.send(response);
  });
});




/************ LOGFILES HANDLER ************/


// Route to get all logfiles
router.get('/logfiles/', (req, res) => {

  if (!logfilesObject.message)
    getLogFilesAndStats();

  res.send({ success: true, message: logfilesObject });
  logfilesObject = [];
  return;
});


// Route to get specific logfile
router.get('/logfiles/:file', (req, res) => {
  // Don't need to check for req.params.file -> no name provided, another handler handles it
  var file = __dirname + '/../logfiles/' + req.params.file;

  // Checks if file exists
  if (fs.existsSync(file))
    res.download(file);
  else
    return res.send({ success: false, message: req.params.file + " not found!" });
});


// Function to get all file + date stat
var logfilesObject = [];

function getLogFilesAndStats() {
  var files = fs.readdir('./logfiles/', (error, files) => {
    // loops through each file
    files.forEach((file) => {
      // Gets stat from specific file
      fs.stat('./logfiles/' + file, (error, statsObject) => {
        logfilesObject.push(({ filename: file, created: statsObject.ctime }));
      });
    });
  });
}

// Call function immediately to fix empty array bug
getLogFilesAndStats();


// File Delete handler
router.delete('/logfiles/:filename', (req, res) => {
  // Checks if Authorized
  Authorized(req, (response) => {
    if (response.success) {
      // Don't need to check for req.params.file -> no name provided, another handler handles it
      var file = __dirname + '/../logfiles/' + req.params.filename;
      // Checks if file exists
      if (fs.existsSync(file)) {
        // Deletes the File
        fs.unlink(file , (error) => {
          if (error) {
            log("Deleting File Error: " + error);
            return res.send({ success: false, message: "Error: " + error});
          } else {
            log(req.params.filename + " was successfully deleted");
            return res.send({ success: true, message: req.params.filename + " was successfully deleted" });
          }
        });
      } else
        return res.send({ success: false, message: req.params.filename + " not found!" });
    } else
      return res.send(response);
  });
});




/************ USER HANDLER ************/


// Authenticate POST handler
router.post('/user/authenticate', (req, res) => {
  // calls login function
  UserController.login(req.body.username, req.body.password, (response) => {
    if (response.success) {
      req.session.username = response.username;
      req.session.authToken = response.token;
      log(response.username + " -> " + response.message);
      return res.send({ success: response.success, username: response.username, message: response.message });
    } else
      return res.send(response);
  });
});


// Register POST handler
router.post('/user/register', (req, res) => {
  // calls register function
  UserController.register(req.body.username, req.body.password, (response) => {
    if (response.success)
      log(response.message);

    return res.send(response);
  });
});


// Handle logout -> destroy session
router.get('/user/logout', (req, res) => {
  req.session.destroy((error) => {
    if (error) log(error);
    return res.send({ success: true, message: "Logout successful" });
  });
});


// Get all Users from DB
router.get('/user', (req, res) => {
  // Checks if Authorized
  Authorized(req, (response) => {
    if (response.success) {
      // gets all users from db
      UserController.getAllUsers((response) => {
        return res.send(response);
      });
    } else
      return res.send(response);
  });

});


// Edit User handler
router.put('/user', (req, res) => {
  // Checks if Authorized
  Authorized(req, (response) => {
    if (response.success) {
      // Updates User with new Password and checks callback
      UserController.updateUser(req.body.id, req.body.newPassword, (response) => {
        log(response.message);
        return res.send(response);
      });
    } else
      return res.send(response);
  });

});


// Delete User Handler
router.delete('/user/:id/:username', (req, res) => {
  // Checks if Authorized
  Authorized(req, (response) => {
    if (response.success) {
      // calls deleteUser function
      UserController.deleteUser(req.params.id, req.params.username, (response) => {
        log(response.message);
        return res.send(response);
      });
    } else
      return res.send(response);
  });

});


// Route to check if user is logged in or not
router.get('/user/loginstate', (req, res) => {
  if (req.session.username && req.session.authToken)
    return res.send({ success: true, message: 'User logged in', username: req.session.username });

  return res.send({ success: false, message: 'User not logged in!', username: false })
})




/************ WEBSOCKET URL HANDLER ************/


// GET Websocket URL
var id = -1;
router.get('/websocket', (req, res) => {
  // Checks if Authorized
  Authorized(req, (response) => {
    if (response.success)
      return res.send({ success: true, message: "Token valid, authorized url was sent!" , url: settings.websocketIP + "/?username=" + response.message});
    else {
      id++;
      return res.send({ success: true, message: response.message , url: settings.websocketIP + "/?username=Tempuser" +id });
    }

  });

});



/************ ERROR HANDLER ************/


// Watch out for possible undefined route requests
router.get('*', (req, res) => {
  return res.status(404).send({ success: false, message: "Route/Method was not found on API" });
});

// Watch out for possible undefined route requests
router.post('*', (req, res) => {
  return res.status(404).send({ success: false, message: "Route/Method was not found on API" });
});

// Watch out for possible undefined route requests
router.delete('*', (req, res) => {
  return res.status(404).send({ success: false, message: "Route/Method was not found on API" });
});

// Watch out for possible undefined route requests
router.put('*', (req, res) => {
  return res.status(404).send({ success: false, message: "Route/Method was not found on API" });
});



/************ HELPER FUNCTIONS ************/


// Log function
function log(message) {
	message = '[' + new Date().toLocaleString() + '] ' + message;	// Add Date+Time to message
	// Add message to File -> if success write it to console
	fs.appendFile('./logFile.log', message + '\n', (error) => {
		if (error) throw(error);
		else console.log(message);
	});
}


// function to check if user is authoried through session
function Authorized(req, callback) {
  if (req.session.authToken) {
    UserController.verifyToken(req.session.authToken, (response) => {
      return callback(response);
    });
  } else
    return callback({ succes: false, message: "No Authorization Token provided!" });
}


// export all these functions
module.exports = router;
